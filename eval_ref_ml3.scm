(load "./utils.scm")

;; Location

(define (function-app? expr)
  (and (list? expr) (= (length expr) 2)))

(define (gen-new-loc)
  (gensym "@l"))

;; eval returns (val . store)
(define (eval-ml expr env store)
  (cond [(integer? expr) (values expr store)]
	[(eq? 'true expr) (values expr store)]
	[(eq? 'false expr) (values expr store)]
	[(symbol? expr) (values (get-assoc expr env) store)]
	[(tagged-list? 'loc expr)
	 (values expr store)]
	[(tagged-list? 'if expr)
	 (let ([e1 (nth 1 expr)]
	       [e2 (nth 2 expr)]
	       [e3 (nth 3 expr)])
	   (receive [test store] (eval-ml e1 env store)
		    (if (eq? 'true test)
			(eval-ml e2 env store)
			(eval-ml e3 env store))))]
	[(any-of-tagged-list? expr '+ '- '* '<)
	 (let ([e1 (nth 1 expr)]
	       [e2 (nth 2 expr)])
	   (receive [v1 store] (eval-ml e1 env store)
		    (receive [v2 store] (eval-ml e2 env store)
			     (values (if (tagged-list? '< expr)
					 (if (< v1 v2) 'true 'false)
					 ((symbol->proc (nth 0 expr)) v1 v2))
				     store))))]
	[(tagged-list? 'let expr)
	 (let* ([bind (nth 1 expr)]
		[x (nth 0 bind)]
		[e1 (nth 1 bind)]
		[e2 (nth 2 expr)])
	   (receive [v1 store] (eval-ml e1 env store)
		    (eval-ml e2 (cons (cons x v1) env) store)))]
	[(tagged-list? 'fun expr)
	 (let* ([x (nth 1 expr)]
		[e (nth 2 expr)])
	   (values (list 'close env x e) store))]
	[(tagged-list? 'ref expr)
	 (let* ([e (nth 1 expr)]
		[loc (gen-new-loc)])
	   (receive [v store] (eval-ml e env store)
		    (values loc (cons (cons loc v) store))))]
	[(tagged-list? 'deref expr)
	 (let* ([e (nth 1 expr)])
	   (receive [v store] (eval-ml e env store)
		    (values (get-assoc v store) store)))]
	[(tagged-list? 'assign expr)
	 (let* ([e1 (nth 1 expr)]
		[e2 (nth 2 expr)])
	   (receive [l store] (eval-ml e1 env store)
		    (receive [v store] (eval-ml e2 env store)
			     (values v (update store l v)))))]
	[(tagged-list? 'letrec expr)
	 (let* ([bind (nth 1 expr)]
		[x (nth 0 bind)]
		[rec-fn (nth 1 bind)]
		[e2 (nth 2 expr)])
	   (eval-ml e2
		    (cons (cons x (list 'rec env x rec-fn))
			  env)
		    store))]
	[(function-app? expr)
	 (receive [fn-v store] (eval-ml (nth 0 expr) env store)
		  (receive [arg-v store] (eval-ml (nth 1 expr) env store)
			   (cond [(tagged-list? 'close fn-v)
				  (let* ([fn-env (nth 1 fn-v)]
					 [fn-var (nth 2 fn-v)]
					 [fn-body (nth 3 fn-v)])
				    (eval-ml fn-body
					     (cons (cons fn-var arg-v) fn-env)
					     store))]
				 [(tagged-list? 'rec fn-v)
				  (let* ([fn-env (nth 1 fn-v)]
					 [self-name (nth 2 fn-v)]
					 [fn-fn (nth 3 fn-v)]
					 [y (nth 1 fn-fn)]
					 [e0 (nth 2 fn-fn)])
				    (eval-ml e0
					     (cons (cons y arg-v)
						   (cons (cons self-name fn-v)
							 fn-env))
					     store))])))]))

(eval-ml '(+ (deref x) 3) '((x . @l)) '((@l . 2)))
(eval-ml '(assign x (+ (deref x) 1))
	 '((x . @l))
	 '((@l . 2)))
(eval-ml '(let (r (ref true))
	    (deref r))
	 '()
	 '())
(eval-ml '(let (incr (fun x (assign x (+ (deref x) 1))))
	    (let (x (ref 0))
	      (let (z (incr x))
		(deref x))))
	 '()
	 '())
(eval-ml '(let (c (let (x (ref 0))
		    (fun y (if y
			       (assign x (+ (deref x) 1))
			       (deref x)))))
	    (let (y (c true))
	      (let (y (c true))
		(c false))))
	 '()
	 '())
(eval-ml '(let (newc (fun x (let (x (ref x))
			      (fun y (if y
					 (assign x (+ (deref x) 1))
					 (deref x))))))
	    (let (c1 (newc 5))
	      (let (c2 (newc 4))
		(let (y (c1 true))
		  (let (y (c2 true))
		    (c1 false))))))
	 '()
	 '())
(eval-ml '(let (f (fun r1 (fun r2 (let (z (assign r2 3))
				    (deref r1)))))
	    (let (r (ref 0))
	      ((f r) r)))
	 '()
	 '())
(eval-ml '(let (x (ref 2))
	    (let (y (ref 3))
	      (let (refx (ref x))
		(let (refy (ref y))
		  (let (z (assign (deref refx)
				  (deref (deref refy))))
		    (deref x))))))
	 '()
	 '())
(eval-ml '(let (f (ref (fun x x)))
	    (let (fact (fun n (if (< n 1) 1 (* n ((deref f) (- n 1))))))
	      (let (z (assign f fact))
		(fact 3))))
	 '()
	 '())
(eval-ml '(letrec (do (fun f (fun i
				  (if (< i 1)
				      0
				      (let (x (f i))
					((do f) (- i 1)))))))
	    (let (x (ref 0))
	      (let (sum (fun i (assign x (+ (deref x) i))))
		(let (y ((do sum) 3))
		  (deref x)))))
	 '()
	 '())

(define (make-evalto S1 env e v S2)
  (list 'evalto S1 env e v S2))

;; S1/env |- e evalto v/S2
'(evalto S1 env e v S2)
;; TODO: this not works. eval-ml returns fresh @l every time and results bad derivaration.
(define (derive-evalto judge)
  (let ([S (nth 1 judge)]
	[env (nth 2 judge)]
	[e (nth 3 judge)]
	[v (nth 4 judge)]
	[S-res (nth 5 judge)])
    (cond [(integer? e)
	   `(derived ,judge (E-Int))]
	  [(or (eq? e 'true) (eq? e 'false))
	   `(derived ,judge (E-Bool))]
	  [(tagged-list? 'if e)
	   (let* ([e1 (nth 1 e)]
		  [e2 (nth 2 e)]
		  [e3 (nth 3 e)])
	     (receive [test S2] (eval-ml e1 env S)
		      (if (eq? 'true test)
			  (receive [v S3] (eval-ml e2 env S2)
				   `(derived ,judge
					     (E-IfT ,(derive (make-evalto S env e1 'true S2))
						    ,(derive (make-evalto S2 env e2 v S3)))))
			  (receive [v S3] (eval-ml e3 env S2)
				   `(derived ,judge
					     (E-IfF ,(derive (make-evalto S env e1 'false S2))
						    ,(derive (make-evalto S2 env e3 v S3))))))))]
	  [(any-of-tagged-list? e '+ '- '* '<)
	   (let* ([e1 (nth 1 e)]
		  [e2 (nth 2 e)])
	     (receive [i1 S2] (eval-ml e1 env S)
		      (receive [i2 S3] (eval-ml e2 env S2)
			       (let* ([op (nth 0 e)]
				      [op-fn (symbol->proc op)]
				      [rule (get-assoc op '((+ . E-Plus) (- . E-Minus) (* . E-Mult) (< . E-Lt)))]
				      [b-name (get-assoc op '((+ . plus) (- . minus) (* . times) (< . less)))])
				 `(derived ,judge
					   (,rule
					    ,(derive (make-evalto S env e1 i1 S2))
					    ,(derive (make-evalto S2 env e2 i2 S3))
					    ,(derive (list b-name i1 i2 (if (eq? op '<)
									    (if (< i1 i2) 'true 'false)
									    (op-fn i1 i2))))))))))]
	  [(symbol? e)
	   `(derived ,judge (E-Var))]
	  [(tagged-list? 'let e)
	   (let* ([bind (nth 1 e)]
		  [x (nth 0 bind)]
		  [e1 (nth 1 bind)]
		  [e2 (nth 2 e)])
	     (receive [v1 S2] (eval-ml e1 env S)
		      (let* ([newenv (cons (cons x v1) env)])
			(receive [_ S3] (eval-ml e2 newenv S2)
				 `(derived ,judge
					   (E-Let ,(derive (make-evalto S env e1 v1 S2))
						  ,(derive (make-evalto S2 newenv e2 v S3))))))))]
	  [(tagged-list? 'letrec e)
	   (let* ([bind (nth 1 e)]
		  [x (nth 0 bind)]
		  [fn (nth 1 bind)]
		  [rec (list 'rec env x fn)])
	     `(derived ,judge
		       (E-LetRec ,(derive (make-evalto S
						       (cons (cons x rec) env)
						       (nth 2 e)
						       v
						       S-res)))))]
	  [(tagged-list? 'ref e)
	   (let* ([e (nth 1 e)])
	     (receive [v S2] (eval-ml e env S)
		      `(derived ,judge
				(E-Ref ,(derive (make-evalto S env e v S2))))))]
	  [(tagged-list? 'deref e)
	   (let* ([e (nth 1 e)])
	     (receive [l S2] (eval-ml e env S)
		      `(derived ,judge
				(E-Deref ,(derive (make-evalto S env e l S2))))))]
	  [(tagged-list? 'assign e)
	   (let ([e1 (nth 1 e)]
		 [e2 (nth 2 e)])
	     (receive [l S2] (eval-ml e1 env S)
		      (receive [v S3] (eval-ml e2 env S2)
			       `(derived ,judge
					 (E-Assign ,(derive (make-evalto S env e1 l S2))
						   ,(derive (make-evalto S2 env e2 v S3)))))))]
	  [(tagged-list? 'fun e)
	   `(derived ,judge (E-Fun))]
	  [(function-app? e)
	   (let ([e1 (nth 0 e)]
		 [e2 (nth 1 e)])
	     (receive [fn-val S2] (eval-ml e1 env S)
		      (receive [v2 S3] (eval-ml e2 env S2)
			       (if (tagged-list? 'close fn-val)
				   (let* ([env2 (nth 1 fn-val)]
					  [x (nth 2 fn-val)]
					  [e0 (nth 3 fn-val)]
					  [newenv (cons (cons x v2) env2)])
				     (receive [_ S4] (eval-ml e0 newenv S3)
				      `(derived ,judge
						(E-App
						 ,(derive (make-evalto S env e1 fn-val S2))
						 ,(derive (make-evalto S2 env e2 v2 S3))
						 ,(derive (make-evalto S3
								       newenv
								       e0
								       v
								       S4))))))
				   (let* ([env2 (nth 1 fn-val)]
					  [x (nth 2 fn-val)]
					  [fn-rec (nth 3 fn-val)]
					  [y (nth 1 fn-rec)]
					  [e0 (nth 2 fn-rec)]
					  [newenv (cons (cons y v2)
							(cons (cons x fn-val)
							      env2))])
				     (receive [_ S4] (eval-ml e0 newenv S3)
					      `(derived ,judge
							(E-AppRec
							 ,(derive (make-evalto S env e1 fn-val S2))
							 ,(derive (make-evalto S2 env e2 v2 S3))
							 ,(derive (make-evalto S3
									       newenv
									       e0
									       v
									       S4))))))))))]
	  [else `(derived ,judge ?)])))

(define (derive judge)
  (cond [(tagged-list? 'evalto judge)
	 (derive-evalto judge)]
	[(tagged-list? 'plus judge)
	 `(derived ,judge (B-Plus))]
	[(tagged-list? 'minus judge)
	 `(derived ,judge (B-Minus))]
	[(tagged-list? 'times judge)
	 `(derived ,judge (B-Mult))]
	[(tagged-list? 'less judge)
	 `(derived ,judge (B-Lt))]
	[else
	 `(derived ,judge ?)]))

(define (print-arith e)
  (cond ((or (tagged-list? '* e) (tagged-list? '+ e) (tagged-list? '- e) (tagged-list? '< e))
	 (let ((e1 (nth 1 e))
	       (e2 (nth 2 e)))
	   (display "(")
	   (print-arith e1)
	   (display " ")
	   (display (car e))
	   (display " ")
	   (print-arith e2)
	   (display ")")))
	((tagged-list? 'deref e)
	 (display "!")
	 (print-arith (nth 1 e)))
	((tagged-list? 'ref e)
	 (display "ref (")
	 (print-arith (nth 1 e))
	 (display ")"))
	((tagged-list? 'assign e)
	 (print-arith (nth 1 e))
	 (display " := ")
	 (print-arith (nth 2 e)))
	((tagged-list? 'if e)
	 (let ((e1 (nth 1 e))
	       (e2 (nth 2 e))
	       (e3 (nth 3 e)))
	   (display "(if ")
	   (print-arith e1)
	   (display " then ")
	   (print-arith e2)
	   (display " else ")
	   (print-arith e3)
	   (display ")")))
	((any-of-tagged-list? e 'let 'letrec)
	 (let* ((bind (nth 1 e))
		(x (nth 0 bind))
		(e1 (nth 1 bind))
		(e2 (nth 2 e)))
	   (if (tagged-list? 'let e)
	       (display "(let ")
	       (display "(let rec "))
	   (display x)
	   (display " = ")
	   (print-arith e1)
	   (display " in ")
	   (print-arith e2)
	   (display ")")))
	((tagged-list? 'fun e)
	 (display "fun ")
	 (display (nth 1 e))
	 (display " -> ")
	 (print-arith (nth 2 e))
	 (display ""))
	((tagged-list? 'cons e)
	 (display "((")
	 (print-arith (nth 1 e))
	 (display ") :: (")
	 (print-arith (nth 2 e))
	 (display "))"))
	((tagged-list? 'close e)
	 (display "(")
	 (print-env (nth 1 e))
	 (display ")")
	 (display "[")
	 (print-arith (list 'fun (nth 2 e) (nth 3 e)))
	 (display "]"))
	((tagged-list? 'rec e)
	 (let ((env (nth 1 e))
	       (name (nth 2 e))
	       (fn (nth 3 e)))
	   (display "(")
	   (print-env env)
	   (display ")[rec ")
	   (display name)
	   (display " = ")
	   (print-arith fn)
	   (display "]")))
	((tagged-list? 'match e)
	 (display "(match ")
	 (print-arith (nth 1 e))
	 (display " with [] -> ")
	 (print-arith (nth 2 e))
	 (display " | ")
	 (display (nth 0 (nth 3 e)))
	 (display "::")
	 (display (nth 1 (nth 3 e)))
	 (display " -> ")
	 (print-arith (nth 4 e))
	 (display ")"))
	((list? e)
	 (display "(")
	 (print-arith (car e))
	 (display " (")
	 (print-arith (cadr e))
	 (display "))"))
	((equal? 'nil e)
	 (display "[]"))
	(else
	 (display e))))

(define (print-env env)
  (if (null? env)
      '()
      (let loop ((env (reverse env)))
	  (let* ((vv (car env))
		 (var (car vv))
		 (val (cdr vv)))
	    (display var)
	    (display " = ")
	    (print-arith val)
	    (if (null? (cdr env))
		'()
		(begin
		  (display " , ")
		  (loop (cdr env))))))))

(define (print-judge judge)
  (cond [(tagged-list? 'evalto judge)
	 (let ([S (nth 1 judge)]
	       [env (nth 2 judge)]
	       [e (nth 3 judge)]
	       [v (nth 4 judge)]
	       [S-res (nth 5 judge)])
	   (when (not (null? S))
		 (print-env S)
		 (display " / "))
	   (print-env env)
	   (display " |- ")
	   (print-arith e)
	   (display " evalto ")
	   (print-arith v)
	   (when (not (null? S-res))
		 (display " / ")
		 (print-env S-res)))]
	[else (let ((op (car judge))
		    (n1 (cadr judge))
		    (n2 (caddr judge))
		    (n3 (cadddr judge)))
		(display n1)
		(display " ")
		(if (eq? op 'less)
		    (display " less than ")
		    (display op))
		(display " ")
		(display n2)
		(display " is ")
		(display n3))]))

(define (make-answer judge)
  (println judge)
  (println "====================")
  (print-derived print-judge (derive judge))
  (newline)
  (println "===================="))

(define q141 (make-evalto '((@l . 2))
			  '((x . @l))
			  '(+ (deref x) 3)
			  5
			  '((@l . 2))))
(define q142 (make-evalto '((@l . 2))
			  '((x . @l))
			  '(assign x (+ (deref x) 1))
			  3
			  '((@l . 3))))
(define q143 (make-evalto '()
			  '()
			  '(let (r (ref true))
			     (deref r))
			  'true
			  '((@l . true))))
(define q144 (make-evalto '()
			  '()
			  '(let (incr (fun x (assign x (+ (deref x) 1))))
			     (let (x (ref 0))
			       (let (z (incr x))
				 (deref x))))
			  1
			  '((@l . 1))))
(define q145 (make-evalto '()
			  '()
			  '(let (c (let (x (ref 0))
				     (fun y (if y
						(assign x (+ (deref x) 1))
						(deref x)))))
			     (let (y (c true))
			       (let (y (c true))
				 (c false))))
			  2
			  '((@l . 2))))
(define q146 (make-evalto '()
			  '()
			  '(let (newc (fun x (let (x (ref x))
					       (fun y (if y
							  (assign x (+ (deref x) 1))
							  (deref x))))))
			     (let (c1 (newc 5))
			       (let (c2 (newc 4))
				 (let (y (c1 true))
				   (let (y (c2 true))
				     (c1 false))))))
			  6
			  (reverse '((@l1  . 6)
				     (@l2 . 5)))))
(define q147 (make-evalto '()
			  '()
			  '(let (f (fun r1 (fun r2 (let (z (assign r2 3))
				    (deref r1)))))
			     (let (r (ref 0))
			       ((f r) r)))
			  3
			  '((@l . 3))))
(define q149 (make-evalto '()
			  '()
			  '(let (f (ref (fun x x)))
			     (let (fact (fun n (if (< n 1) 1 (* n ((deref f) (- n 1))))))
			       (let (z (assign f fact))
				 (fact 3))))
			  6
			  `((@l1 . ,(list 'close '((f . @l1)) 'n '(if (< n 1) 1 (* n ((deref f) (- n 1)))))))))

(define q150 (make-evalto '()
			  '()
			  '(letrec (do (fun f (fun i
						   (if (< i 1)
						       0
						       (let (x (f i))
							 ((do f) (- i 1)))))))
			     (let (x (ref 0))
			       (let (sum (fun i (assign x (+ (deref x) i))))
				 (let (y ((do sum) 3))
				   (deref x)))))
			  6
			  '((@l . 6))))

(define q148 (make-evalto '()
			  '()
			  '(let (x (ref 2))
			     (let (y (ref 3))
			       (let (refx (ref x))
				 (let (refy (ref y))
				   (let (z (assign (deref refx)
						   (deref (deref refy))))
				     (deref x))))))
			  3
			  (reverse'((@l1 . 3) (@l2 . 3) (@l3 . @l1) (@l4 . @l2)))))

(map make-answer (list q141 q142 q143 q144 q145 q147 q149 q150 q146 q148))


(derive (make-evalto '((@l72 . 0))
		     '((x . @l72) (incr . (close () x (assign x (+ (deref x) 1)))))
		     '(incr x)
		     1
		     '((@l72 . 0))))
