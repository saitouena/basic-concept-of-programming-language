(load "./utils.scm")


'(>> expr k)
'(=> v k)
'(cont (if _ 1 3)) ;; {if _ then 1 else 3}

;; -2 >> {_*8} >> {-3 < _} >> {if _ then 8 else 2} >> {3 + _} >> {_ + 4}
'(>> -2 ((* _ 8) (< -3 _) (if _ 8 2) (+ 3 _) (+ _ 4) _))

'(evalto expr val)

(define (make-evalto expr val)
  (list 'evalto expr val))

(define (make=> expr conts)
  (list '=> expr conts))

(define (make>> expr conts)
  (list '>> expr conts))

(define (derive-evalto judge)
  (let ([expr (nth 1 judge)]
	[res-val (nth 2 judge)])
    (cond [(tagged-list? '>> expr)
	   (let* ([e (nth 1 expr)]
		  [conts (nth 2 expr)]
		  [top (car conts)])
	     (cond [(integer? e)
		    `(derived ,judge
			      (E-Int
			       ,(derive (make-evalto (make=> e conts) res-val))))]
		   [(or (eq? e 'true) (eq? e 'false))
		    `(derived ,judge
			      (E-Bool
			       ,(derive (make-evalto (make=> e conts) res-val))))]
		   [(any-of-tagged-list? e '+ '< '- '*)
		    (let [(op (nth 0 e))
			  (e1 (nth 1 e))
			  (e2 (nth 2 e))]
		      `(derived ,judge
				(E-BinOp
				 ,(derive (make-evalto (make>> e1 (cons `(,op _ ,e2)
									conts))
						       res-val)))))]
		   [(tagged-list? 'if e)
		    (let [(e1 (nth 1 e))
			  (e2 (nth 2 e))
			  (e3 (nth 3 e))]
		      `(derived ,judge
				(E-If
				 ,(derive (make-evalto (make>> e1 (cons `(if _ ,e2 ,e3)
									conts))
						       res-val)))))]
		   [else (error "derive-evalto judge=" judge)]))]
	  [(tagged-list? '=> expr)
	   (let* ([v (nth 1 expr)]
		  [conts (nth 2 expr)]
		  [top (car conts)])
	     (cond [(eq? top '_)
		    `(derived ,judge (C-Ret))]
		   [(any-of-tagged-list? top '+ '- '* '<)
		    (if (eq? (nth 1 top) '_)
			(let ([e (nth 2 top)]
			      [op (nth 0 top)])
			  `(derived ,judge
				    (C-EvalR ,(derive (make-evalto (make>> e (cons `(,op ,v _)
										   (cdr conts)))
								   res-val)))))
			(let* ([i1 (nth 1 top)]
			       [op (nth 0 top)]
			       [op-fn (symbol->proc op)]
			       [rule (get-assoc op '((+ . C-Plus) (- . C-Minus) (* . C-Times) (< . C-Lt)))]
			       [b-name (get-assoc op '((+ . plus) (- . minus) (* . times) (< . less)))]
			       [i3 (if (eq? op '<)
				       (if (< i1 v)
					   'true
					   'false)
				       (op-fn i1 v))])
			  `(derived ,judge
				    (,rule
				     ,(derive `(,b-name ,i1 ,v ,i3))
				     ,(derive (make-evalto (make=> i3 (cdr conts))
							   res-val))))))]
		   [(tagged-list? 'if top)
		    (if (eq? 'true v)
			`(derived ,judge
				  (C-IfT ,(derive (make-evalto (make>> (nth 2 top) (cdr conts))
							       res-val))))
			`(derived ,judge
				  (C-IfF ,(derive (make-evalto (make>> (nth 3 top) (cdr conts))
							       res-val)))))]
		   [else (error "derive-evalto judge=" judge)]))])))

(define (derive judge)
  (cond ((tagged-list? 'evalto judge)
	 (derive-evalto judge))
	((tagged-list? 'plus judge)
	 `(derived ,judge (B-Plus)))
	((tagged-list? 'minus judge)
	 `(derived ,judge (B-Minus)))
	((tagged-list? 'times judge)
	 `(derived ,judge (B-Times)))
	((tagged-list? 'less judge)
	 `(derived ,judge (B-Lt)))))

(define (print-arith e)
  (cond ((or (tagged-list? '* e) (tagged-list? '+ e) (tagged-list? '- e) (tagged-list? '< e))
	 (let ((e1 (nth 1 e))
	       (e2 (nth 2 e)))
	   (display "(")
	   (print-arith e1)
	   (display " ")
	   (display (car e))
	   (display " ")
	   (print-arith e2)
	   (display ")")))
	((tagged-list? 'if e)
	 (let ((e1 (nth 1 e))
	       (e2 (nth 2 e))
	       (e3 (nth 3 e)))
	   (display "(if ")
	   (print-arith e1)
	   (display " then ")
	   (print-arith e2)
	   (display " else ")
	   (print-arith e3)
	   (display ")")))
	(else (display e))))

(define (print-arith-no-paren e)
  (cond ((or (tagged-list? '* e) (tagged-list? '+ e) (tagged-list? '- e) (tagged-list? '< e))
	 (let ((e1 (nth 1 e))
	       (e2 (nth 2 e)))
	   (print-arith e1)
	   (display " ")
	   (display (car e))
	   (display " ")
	   (print-arith e2)
))
	((tagged-list? 'if e)
	 (let ((e1 (nth 1 e))
	       (e2 (nth 2 e))
	       (e3 (nth 3 e)))
	   (display "if ")
	   (print-arith e1)
	   (display " then ")
	   (print-arith e2)
	   (display " else ")
	   (print-arith e3)
	   (display "")))
	(else (display e))))

(define (print-cont cont)
  (if (list? cont)
      (begin (display "{")
	     (print-arith-no-paren cont)
	     (display "}"))
      (display cont)))

(define (print-conts-loop conts sep)
  (if (null? conts)
      #t
      (begin (display " ")
	     (display sep)
	     (display " ")
	     (print-cont (car conts))
	     (print-conts-loop (cdr conts) sep))))

(define (print>> expr)
  (print-arith (nth 1 expr))
  (print-conts-loop (nth 2 expr) ">>"))

(define (print=> expr)
  (print-arith (nth 1 expr))
  (display " => ")
  (print-cont (car (nth 2 expr)))
  (print-conts-loop (cdr (nth 2 expr)) ">>"))

(define (print-judge judge)
  (cond [(tagged-list? 'evalto judge)
	 (if (tagged-list? '>> (nth 1 judge))
	     (print>> (nth 1 judge))
	     (print=> (nth 1 judge)))
	 (display " evalto ")
	 (display (nth 2 judge))]
	[else
	 (display (nth 1 judge))
	 (display " ")
	 (display (if (eq? (nth 0 judge) 'less)
		      "less than"
		      (nth 0 judge)))
	 (display " ")
	 (display (nth 2 judge))
	 (display " is ")
	 (display (nth 3 judge))]))

(define (make-answer judge)
  (print judge)
  (println "=================================")
  (print-derived print-judge (derive judge))
  (newline)
  (println "================================="))

(map make-answer (list (make-evalto '(>> 3 (_)) 3)
		       (make-evalto '(>> 5 ((+ 3 _) _)) 8)
		       (make-evalto '(>> (+ 3 5) (_)) 8)
		       (make-evalto '(>> (* (+ 4 5) (- 1 10)) (_)) -81)
		       (make-evalto '(>> (if (< 4 5) (+ 2 3) (* 8 8)) (_)) 5)
		       (make-evalto '(>> (+ (+ 3
					       (if (< -3 (* -2 8)) 8 2))
					    4)
					 (_))
				    9)
		       ))

(derive (make-evalto '(>> (+ (+ 3 (if (< -3 (* -2 8)) 8 2)) 4) (_)) 9))
