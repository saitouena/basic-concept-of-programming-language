(load "./utils.scm")

(define (lookup-environment var env)
  (let loop ((n 1) (env env))
    (if (null? env)
	(error "lookup-environment: var not found. var=" var ",env=" env)
	(if (eq? var (car env))
	    n
	    (loop (+ n 1) (cdr env))))))

(define (transform-to-nameless expr env)
  (cond ((symbol? expr)
	 `(sharp ,(lookup-environment expr env)))
	((any-of-tagged-list? expr '+ '- '< '*)
	 `(,(car expr) ,(transform-to-nameless (nth 1 expr) env) ,(transform-to-nameless (nth 2 expr) env)))
	((tagged-list? 'if expr)
	 `(if ,(transform-to-nameless (nth 1 expr) env)
	      ,(transform-to-nameless (nth 2 expr) env)
	      ,(transform-to-nameless (nth 3 expr) env)))
	((integer? expr)
	 expr)))

;; よくよく考えたら評価機いらんな...（推論規則の上に新しい文字が一切ないので)

;; judge form
'(=> env expr db-expr) ;; De-Bruijn Expression

(define (make-judge env expr db-expr)
  (list '=> env expr db-expr))

(define (derive judge)
  (let ((env (nth 1 judge))
	(expr (nth 2 judge))
	(db-expr (nth 3 judge)))
    (cond ((integer? expr)
	   `(derived ,judge (Tr-Int)))
	  ((any-of? expr 'true 'false)
	   `(derived ,judge (Tr-Bool)))
	  ((tagged-list? 'if expr)
	   `(derived ,judge (Tr-If
			     ,(derive (make-judge env (nth 1 expr) (nth 1 db-expr)))
			     ,(derive (make-judge env (nth 2 expr) (nth 2 db-expr)))
			     ,(derive (make-judge env (nth 3 expr) (nth 3 db-expr))))))
	  ((any-of-tagged-list? expr '+ '- '* '<)
	   `(derived ,judge (,(get-assoc (nth 0 expr) '((+ . Tr-Plus) (- . Tr-Minus) (* . Tr-Times) (< . Tr-Lt)))
			     ,(derive (make-judge env (nth 1 expr) (nth 1 db-expr)))
			     ,(derive (make-judge env (nth 2 expr) (nth 2 db-expr))))))
	  ((tagged-list? 'let expr)
	   (let* ((bind (nth 1 expr))
		  (x (car bind))
		  (e1 (cdr bind))
		  (e2 (nth 2 expr))
		  (d1 (nth 1 db-expr))
		  (d2 (nth 2 db-expr)))
	     `(derived ,judge (Tr-Let
			       ,(derive (make-judge env e1 d1))
			       ,(derive (make-judge (cons x env) e2 d2))))))
	  ((tagged-list? 'letrec expr)
	   (let* ((bind (nth 1 expr))
		  (x (car bind))
		  (fun (cdr bind))
		  (y (nth 1 fun))
		  (e1 (nth 2 fun))
		  (e2 (nth 2 expr))
		  (fun-db (nth 1 db-expr))
		  (d1 (nth 1 fun-db))
		  (d2 (nth 2 db-expr)))
	     `(derived ,judge
		       (Tr-LetRec
			,(derive (make-judge (cons y (cons x env))
					     e1
					     d1))
			,(derive (make-judge (cons x env)
					     e2
					     d2))))))
	  ((tagged-list? 'fun expr)
	   (let* ((x (nth 1 expr))
		  (e (nth 2 expr))
		  (d (nth 1 db-expr)))
	     `(derived ,judge (Tr-Fun
			       ,(derive (make-judge (cons x env) e d))))))
	  ((and (list? expr) (= 2 (length expr)))
	   `(derived ,judge
		     (Tr-App
		      ,(derive (make-judge env (nth 0 expr) (nth 0 db-expr)))
		      ,(derive (make-judge env (nth 1 expr) (nth 1 db-expr))))))
	  ((symbol? expr)
	   (if (eq? expr (car env))
	       `(derived ,judge (Tr-Var1))
	       `(derived ,judge
			 (Tr-Var2 ,(derive (make-judge (cdr env) expr `(sharp ,(- (nth 1 db-expr) 1)))))))))))

(define (print-env env)
  (if (null? env)
      '()
      (let loop ((env env))
	  (let* ((v (car env)))
	    (display v)
	    (if (null? (cdr env))
		'()
		(begin
		  (display " , ")
		  (print-env (cdr env))))))))

(define (print-db-arith e)
  (cond ((or (tagged-list? '* e) (tagged-list? '+ e) (tagged-list? '- e) (tagged-list? '< e))
	 (let ((e1 (nth 1 e))
	       (e2 (nth 2 e)))
	   (display "(")
	   (print-db-arith e1)
	   (display " ")
	   (display (car e))
	   (display " ")
	   (print-db-arith e2)
	   (display ")")))
	((tagged-list? 'if e)
	 (let ((e1 (nth 1 e))
	       (e2 (nth 2 e))
	       (e3 (nth 3 e)))
	   (display "(if ")
	   (print-db-arith e1)
	   (display " then ")
	   (print-db-arith e2)
	   (display " else ")
	   (print-db-arith e3)
	   (display ")")))
	((any-of-tagged-list? e 'let 'letrec)
	 (let* ((e1 (nth 1 e))
		(e2 (nth 2 e)))
	   (if (tagged-list? 'let e)
	       (display "(let ")
	       (display "(let rec "))
	   (display ". = ")
	   (print-db-arith e1)
	   (display " in ")
	   (print-db-arith e2)
	   (display ")")))
	((tagged-list? 'fun e)
	 (display "fun . -> ")
	 (print-db-arith (nth 1 e)))
	((tagged-list? 'close e)
	 (display "(")
	 (print-env (reverse (nth 1 e)))
	 (display ")")
	 (display "[")
	 (print-db-arith (list 'fun (nth 2 e)))
	 (display "]"))
	((tagged-list? 'rec e)
	 (let ((env (rec-env e))
	       (fn (rec-fn e)))
	   (display "(")
	   (print-env (reverse env))
	   (display ")[rec . = ")
	   (print-db-arith fn)
	   (display "]")))
	((tagged-list? 'sharp e)
	 (display "#")
	 (display (cadr e)))
	((list? e)
	 (display "(")
	 (print-db-arith (car e))
	 (display " (")
	 (print-db-arith (cadr e))
	 (display "))"))
	(else
	 (display e))))

(define (print-arith e)
  (cond ((or (tagged-list? '* e) (tagged-list? '+ e) (tagged-list? '- e) (tagged-list? '< e))
	 (let ((e1 (nth 1 e))
	       (e2 (nth 2 e)))
	   (display "(")
	   (print-arith e1)
	   (display " ")
	   (display (car e))
	   (display " ")
	   (print-arith e2)
	   (display ")")))
	((tagged-list? 'if e)
	 (let ((e1 (nth 1 e))
	       (e2 (nth 2 e))
	       (e3 (nth 3 e)))
	   (display "(if ")
	   (print-arith e1)
	   (display " then ")
	   (print-arith e2)
	   (display " else ")
	   (print-arith e3)
	   (display ")")))
	((any-of-tagged-list? e 'let 'letrec)
	 (let* ((bind (nth 1 e))
		(x (car bind))
		(e1 (cdr bind))
		(e2 (nth 2 e)))
	   (if (tagged-list? 'let e)
	       (display "(let ")
	       (display "(let rec "))
	   (display x)
	   (display " = ")
	   (print-arith e1)
	   (display " in ")
	   (print-arith e2)
	   (display ")")))
	((tagged-list? 'fun e)
	 (display "fun ")
	 (display (nth 1 e))
	 (display " -> ")
	 (print-arith (nth 2 e))
	 (display ""))
	((tagged-list? 'close e)
	 (display "(")
	 (print-env (reverse (nth 1 e)))
	 (display ")")
	 (display "[")
	 (print-arith (list 'fun (nth 2 e) (nth 3 e)))
	 (display "]"))
	((tagged-list? 'rec e)
	 (let ((env (rec-env e))
	       (name (rec-name e))
	       (fn (rec-fn e)))
	   (display "(")
	   (print-env (reverse env))
	   (display ")[rec ")
	   (display name)
	   (display " = ")
	   (print-arith fn)
	   (display "]")))
	((list? e)
	 (display "(")
	 (print-arith (car e))
	 (display " (")
	 (print-arith (cadr e))
	 (display "))"))
	(else
	 (display e))))

(define (print-judge judge)
  (let ((env (nth 1 judge))
	(expr (nth 2 judge))
	(db-expr (nth 3 judge)))
    (print-env (reverse env))
    (display " |- ")
    (print-arith expr)
    (display " ==> ")
    (print-db-arith db-expr)))

(define (make-answer input)
  (println "=======================")
  (print-derived print-judge (derive input))
  (newline)
  (println "======================="))

(define (make-input env expr db-expr)
  (list '=> (reverse env) expr db-expr))

(define q54 (make-input '(x y) '(if x (+ y 1) (- y 1)) '(if (sharp 2) (+ (sharp 1) 1) (- (sharp 1) 1))))

(define q56 (make-input '()
			'(let (x . (* 3 3)) (let (y . (* 4 x)) (+ x y)))
			'(let (* 3 3) (let (* 4 (sharp 1)) (+ (sharp 2) (sharp 1))))))

(define q58 (make-input '(x)
			'(let (x . (* x 2)) (+ x x))
			'(let (* (sharp 1) 2)
			   (+ (sharp 1) (sharp 1)))))

(define q60 (make-input '()
			'(let (x . (let (y . (- 3 2)) (* y y)))
			   (let (y . 4)
			     (+ x y)))
			'(let (let (- 3 2) (* (sharp 1) (sharp 1)))
			   (let 4
			       (+ (sharp 2) (sharp 1))))))

(define q62 (make-input '()
			'(let (y . 2)
			   (fun x (+ x y)))
			'(let 2
			     (fun (+ (sharp 1) (sharp 2))))))

(define q64 (make-input '()
			'(let (sm . (fun f (+ (f 3) (f 4))))
			   (sm (fun x (* x x))))
			'(let (fun (+ ((sharp 1) 3) ((sharp 1) 4)))
			   ((sharp 1) (fun (* (sharp 1) (sharp 1)))))))

(define q66 (make-input '()
			'(let (a . 3)
			   (let (f . (fun y (* y a)))
			     (let (a . 5)
			       (f 4))))
			'(let 3
			     (let (fun (* (sharp 1) (sharp 2)))
			       (let 5
				   ((sharp 2) 4))))))

(define q68 (make-input '()
			'(letrec (fact . (fun n
					      (if (< n 2)
						  1
						  (* n (fact (- n 1))))))
			   (fact 3))
			'(letrec (fun (if (< (sharp 1) 2)
					  1
					  (* (sharp 1) ((sharp 2) (- (sharp 1) 1)))))
			   ((sharp 1) 3))))

(map make-answer (list q54 q56 q58 q60 q62 q64 q66 q68))
