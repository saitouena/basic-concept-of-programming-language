(load "./utils.scm")

;; EvalML1 and EvalML1Err

(define (eval-arith e)
  (cond ((any-of-tagged-list? e '+ '* '-)
	 (let ((v1 (eval-arith (nth 1 e)))
	       (v2 (eval-arith (nth 2 e))))
	   (if (or (any-of? 'error v1 v2) (some (lambda (v) (not (integer? v))) v1 v2))
	       'error
	       ((symbol->proc (car e)) v1 v2))))
	((tagged-list? '< e)
	 (let ((v1 (eval-arith (nth 1 e)))
	       (v2 (eval-arith (nth 2 e))))
	   (if (or (any-of? 'error v1 v2) (some (lambda (v) (not (integer? v))) v1 v2))
	       'error
	       (if (< v1 v2)
		   'true
		   'false))))
	((tagged-list? 'if e)
	 (let ((b (eval-arith (nth 1 e)))
	       (th (eval-arith (nth 2 e)))
	       (elt (eval-arith (nth 3 e))))
	   (if (or (any-of? 'error b th elt) (integer? b))
	       'error
	       (if (eq? b 'true)
		   th
		   elt))))
	(else e)))

(define (derive-evalto judge)
  (let ((e (nth 1 judge))
	(i (nth 2 judge)))
    (cond ((any-of-tagged-list? e '+ '- '*)
	   (let* ((op (nth 0 e)) 
		  (e1 (nth 1 e))
		  (e2 (nth 2 e))
		  (i1 (eval-arith e1))
		  (i2 (eval-arith e2)))
	     (cond ((eq? i1 'error)
		    `(derived ,judge
			      (,(get-assoc op '((+ . E-PlusErrorL) (* . E-TimesErrorL) (- . E-MinusErrorL)))
			       ,(derive (list 'evalto e1 'error)))))
		   ((eq? i2 'error)
		    `(derive ,judge
			     (,(get-assoc op '((+ . E-PlusErrorR) (* . E-TimesErrorR) (- . E-MinusErrorR)))
			      ,(derive (list 'evalto e2 'error)))))
		   ((any-of? i1 'true 'false)
		    `(derive ,judge
			     (,(get-assoc op '((+ . E-PlusBoolL) (* . E-TimesBoolL) (- . E-MinusBoolL)))
			      ,(derive (list 'evalto e1 i1)))))
		   ((any-of? i2 'true 'false)
		    `(derive ,judge
			     (,(get-assoc op '((+ . E-PlusBoolR) (* . E-TimesBoolR) (- . E-MinusBoolR)))
			      ,(derive (list 'evalto e2 i2)))))
		   (else`(derived ,judge
				  (,(cond ((eq? op '+) 'E-Plus)
					  ((eq? op '*) 'E-Times)
					  ((eq? op '-) 'E-Minus)
					  (else (error "derive-evalto: unreachable" judge)))
				   ,(derive (list 'evalto e1 i1))
				   ,(derive (list 'evalto e2 i2))
				   ,(derive (list (cond ((eq? op '+) 'plus)
							((eq? op '*) 'times)
							((eq? op '-) 'minus)
							(else (error "derive-evalto: unreachable" judge)))
						  i1
						  i2
						  i))))))))
	  ((tagged-list? '< e)
	   (let* ((e1 (nth 1 e))
		  (e2 (nth 2 e))
		  (i1 (eval-arith e1))
		  (i2 (eval-arith e2)))
	     (cond ((eq? 'error i1)
		    `(derived ,judge
			      (E-LtErrorL
			       ,(derive (list 'evalto e1 'error)))))
		   ((eq? 'error i2)
		    `(derived ,judge
			      (E-LtErrorR
			       ,(derive (list 'evalto e2 'error)))))
		   ((any-of? i1 'true 'false)
		    `(derived ,judge
			      (E-LtBoolL
			       ,(derive (list 'evalto e1 i1)))))
		   ((any-of? i2 'true 'false)
		    `(derived ,judge
			      (E-LtBoolR
			       ,(derive (list 'evalto e2 i2)))))
		   (else
		    `(derived ,judge
			      (E-Lt
			       ,(derive (list 'evalto e1 i1))
			       ,(derive (list 'evalto e2 i2))
			       ,(derive (list 'less i1 i2 (eval-arith (list '< i1 i2))))))))))
	  ((tagged-list? 'if e)
	   (let* ((e1 (nth 1 e))
		  (e2 (nth 2 e))
		  (e3 (nth 3 e))
		  (b (eval-arith e1))
		  (v2 (eval-arith e2))
		  (v3 (eval-arith e3)))
	     (cond ((integer? b)
		    `(derived ,judge
			      (E-IfInt
			       ,(derive (list 'evalto e1 b)))))
		   ((eq? 'error b)
		    `(derived ,judge
			      (E-IfError
			       ,(derive (list 'evalto e1 b)))))
		   (else
		    (if (eq? b 'true)
			`(derived ,judge
				  (,(if (eq? 'error v2) 'E-IfTError 'E-IfT)
				   ,(derive (list 'evalto e1 b))
				   ,(derive (list 'evalto e2 v2))))
			`(derived ,judge
				  (,(if (eq? 'error v3) 'E-IfFError 'E-IfF)
				   ,(derive (list 'evalto e1 b))
				   ,(derive (list 'evalto e3 v3)))))))))
	  (else (if (integer? (nth 1 judge))
		    `(derived ,judge (E-Int))
		    `(derived ,judge (E-Bool)))))))

(define (derive judge)
  (cond ((tagged-list? 'evalto judge)
	 (derive-evalto judge))
	((tagged-list? 'plus judge)
	 `(derived ,judge (B-Plus)))
	((tagged-list? 'minus judge)
	 `(derived ,judge (B-Minus)))
	((tagged-list? 'times judge)
	 `(derived ,judge (B-Times)))
	((tagged-list? 'less judge)
	 `(derived ,judge (B-Lt)))
	(else
	 (error "derive: unknown expression:" judge))))

(define (print-arith e)
  (cond ((or (tagged-list? '* e) (tagged-list? '+ e) (tagged-list? '- e) (tagged-list? '< e))
	 (let ((e1 (nth 1 e))
	       (e2 (nth 2 e)))
	   (display "(")
	   (print-arith e1)
	   (display " ")
	   (display (car e))
	   (display " ")
	   (print-arith e2)
	   (display ")")))
	((tagged-list? 'if e)
	 (let ((e1 (nth 1 e))
	       (e2 (nth 2 e))
	       (e3 (nth 3 e)))
	   (display "(if ")
	   (print-arith e1)
	   (display " then ")
	   (print-arith e2)
	   (display " else ")
	   (print-arith e3)
	   (display ")")))
	(else (display e))))

(define (print-judge judge)
  (cond ((tagged-list? 'evalto judge)
	 (let ((e (nth 1 judge))
	       (n (nth 2 judge)))
	   (print-arith e)
	   (display " evalto ")
	   (display n)))
	;; times, plus, minus, less
	(else (let ((op (car judge))
		    (n1 (cadr judge))
		    (n2 (caddr judge))
		    (n3 (cadddr judge)))
		(display n1)
		(display " ")
		(if (eq? op 'less)
		    (display " less than ")
		    (display op))
		(display " ")
		(display n2)
		(display " is ")
		(display n3)))))

(define (print-derived derived)
  (let ((judge (cadr derived))
	(reason (caddr derived)))
    (print-judge judge)
    (display " by ")
    (let ((rule (car reason))
	  (children-derived (cdr reason)))
      (display rule)
      (display " {")
      (print-children-derived children-derived)
      (display "}"))))

(define (print-children-derived children)
  (if (null? children)
      "{}"
      (let loop ((children children))
	(if (null? (cdr children))
	    (print-derived (car children))
	    (begin (print-derived (car children))
		   (display ";")
		   (print-children-derived (cdr children)))))))
(define (make-answer judge)
  (print-derived (derive judge))
  (newline))

(define q25 '(evalto (+ 3 5) 8))
(define q26 '(evalto (- (- 8 2) 3) 3))
(define q27 '(evalto (* (+ 4 5) (- 1 10)) -81))
(define q28 '(evalto (if (< 4 5) (+ 2 3) (* 8 8)) 5))
(define q29 '(evalto (+ 3
			(if (< -23 (* -2 8))
			    8
			    (+ 2 4)))
		     11))
(define q30 '(evalto (+ (+ 3
			   (if (< -23 (* -2 8))
			       8
			       2))
			4)
		     15))
(define q31 '(evalto (+ (+ 1 true) 2) error))
(define q32 '(evalto (if (+ 2 3) 1 3) error))
(define q33 '(evalto (if (< 3 4) (< 1 true) (- 3 false)) error))

(map make-answer (list q25 q26 q27 q28 q29 q30 q31 q32 q33))
