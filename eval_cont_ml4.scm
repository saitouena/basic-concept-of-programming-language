(load "./utils.scm")


'(>> env expr k)
'(=> v k)
;; k
'((env . (* _ x)) (noenv . (+ 3 _)))

;; let
'(let (x 10) (+ x 10))

;; closure
'(close env var expr)

;; continuation
'(cont ks)


(define (make-evalto expr val)
  (list 'evalto expr val))

(define (make=> val conts)
  (list '=> val conts))

(define (make>> env expr conts)
  (list '>> env expr conts))

(define (make-cont env expr)
  (cons env expr))

(define (make-closure env var expr)
  (list 'close env var expr))

(define (make-rec env self-name fn-expr)
  (list 'rec env self-name fn-expr))

(define (function-app? expr)
  (and (list? expr) (= 2 (length expr))))

(define (make-firstclass-cont conts)
  (list 'cont conts))

(define (derive>> judge)
  (let* ([whole-expr (nth 1 judge)]
	 [env (nth 1 whole-expr)]
	 [expr (nth 2 whole-expr)]
	 [conts (nth 3 whole-expr)]
	 [res-val (nth 2 judge)])
    (cond [(integer? expr)
	   `(derived ,judge
		     (E-Int
		      ,(derive (make-evalto (make=> expr conts) res-val))))]
	  [(or (eq? expr 'true) (eq? expr 'false))
	   `(derived ,judge
		     (E-Bool
		      ,(derive (make-evalto (make=> expr conts) res-val))))]
	  [(eq? expr 'nil)
	   `(derived ,judge
		     (E-Nil ,(derive (make-evalto (make=> expr conts) res-val))))]
	  [(tagged-list? 'cons expr)
	   (let* ([e1 (nth 1 expr)]
		  [e2 (nth 2 expr)])
	     `(derived ,judge
		       (E-Cons ,(derive (make-evalto (make>> env e1 (cons (make-cont env `(cons _ ,e2)) conts))
						     res-val)))))]
	  [(tagged-list? 'match expr)
	   (let* ([e1 (nth 1 expr)]
		  [e2 (nth 2 expr)]
		  [bind (nth 3 expr)]
		  [e3 (nth 4 expr)])
	     `(derived ,judge
		       (E-Match ,(derive (make-evalto (make>> env e1 (cons (make-cont env `(match _ ,e2 ,bind ,e3)) conts))
						     res-val)))))]
	  [(tagged-list? 'if expr)
	   (let* ([e1 (nth 1 expr)]
		  [e2 (nth 2 expr)]
		  [e3 (nth 3 expr)])
	     `(derived ,judge
		       (E-If
			,(derive (make-evalto (make>> env e1
						      (cons (make-cont env `(if _ ,e2 ,e3))
							    conts))
					      res-val)))))]
	  [(any-of-tagged-list? expr '+ '- '* '<)
	   (let* ([op (nth 0 expr)]
		  [e1 (nth 1 expr)]
		  [e2 (nth 2 expr)])
	     `(derived ,judge
		       (E-BinOp
			,(derive (make-evalto (make>> env e1
						      (cons (make-cont env `(,op _ ,e2))
							    conts))
					      res-val)))))]
	  [(symbol? expr)
	   `(derived ,judge
		     (E-Var
		      ,(derive (make-evalto (make=> (get-assoc expr env) conts)
					    res-val))))]
	  [(tagged-list? 'let expr)
	   (let* ([bind (nth 1 expr)]
		  [x (nth 0 bind)]
		  [e1 (nth 1 bind)]
		  [e2 (nth 2 expr)])
	     `(derived ,judge
		       (E-Let
			,(derive (make-evalto (make>> env
						      e1
						      (cons (make-cont env `(let (,x _) ,e2)) conts))
					      res-val)))))]
	  [(tagged-list? 'letrec expr)
	   (let* ([bind (nth 1 expr)]
		  [x (nth 0 bind)]
		  [fn-expr (nth 1 bind)]
		  [y (nth 1 fn-expr)]
		  [e1 (nth 2 fn-expr)]
		  [e2 (nth 2 expr)])
	     `(derived ,judge
		       (E-LetRec
			,(derive (make-evalto (make>> (cons (cons x (make-rec env x (list 'fun y e1))) env)
						      e2
						      conts)
					      res-val)))))]
	  [(tagged-list? 'letcc expr)
	   (let* ([x (nth 1 expr)]
		  [e (nth 2 expr)]
		  [newenv (cons (cons x (make-firstclass-cont conts)) env)])
	     `(derived ,judge
		       (E-LetCc ,(derive (make-evalto (make>> newenv e conts)
						      res-val)))))]
	  [(tagged-list? 'fun expr)
	   (let* ([x (nth 1 expr)]
		  [e (nth 2 expr)])
	     `(derived ,judge
		       (E-Fun ,(derive (make-evalto (make=> (make-closure env x e) conts)
						    res-val)))))]
	  [(function-app? expr)
	   (let ([e1 (nth 0 expr)]
		 [e2 (nth 1 expr)])
	     `(derived ,judge
		       (E-App ,(derive (make-evalto (make>> env e1 (cons (make-cont env `(_ ,e2)) conts))
						    res-val)))))]
	  [else `(derived ,judge ?)])))

(define (derive=> judge)
  (let* ([whole-expr (nth 1 judge)]
	 [v (nth 1 whole-expr)]
	 [conts (nth 2 whole-expr)]
	 [top-cont (car conts)]
	 [res-val (nth 2 judge)])
    (if (eq? top-cont '_)
	`(derived ,judge (C-Ret))
	(let* ([top-cont-expr (cdr top-cont)]
	       [rest-conts (cdr conts)]
	       [top-cont-env (car top-cont)])
	  (cond  [(any-of-tagged-list? top-cont-expr '+ '- '* '<)
		 (if (eq? (nth 1 top-cont-expr) '_)
		     `(derived ,judge (C-EvalR
				       ,(derive (make-evalto (make>> top-cont-env
								     (nth 2 top-cont-expr)
								     (cons (make-cont 'noenv `(,(nth 0 top-cont-expr) ,v _))
									   rest-conts))
							     res-val))))
		     (let* ([i1 (nth 1 top-cont-expr)]
			    [op (nth 0 top-cont-expr)]
			    [op-fn (symbol->proc op)]
			    [rule (get-assoc op '((+ . C-Plus) (- . C-Minus) (* . C-Times) (< . C-Lt)))]
			    [b-name (get-assoc op '((+ . plus) (- . minus) (* . times) (< . less)))]
			    [i3 (if (eq? op '<)
				    (if (< i1 v)
					'true
					'false)
				    (op-fn i1 v))])
		       `(derived ,judge
				 (,rule
				  ,(derive `(,b-name ,i1 ,v ,i3))
				  ,(derive (make-evalto (make=> i3 rest-conts)
							res-val))))))]
		[(tagged-list? 'if top-cont-expr)
		 (if (eq? 'true v)
		     `(derived ,judge
			       (C-IfT ,(derive (make-evalto (make>> top-cont-env (nth 2 top-cont-expr) rest-conts)
							    res-val))))
		     `(derived ,judge
			       (C-IfF ,(derive (make-evalto (make>> top-cont-env (nth 3 top-cont-expr) rest-conts)
							    res-val)))))]
		[(tagged-list? 'cons top-cont-expr)
		 (if (eq? '_ (nth 1 top-cont-expr))
		     `(derived ,judge
			       (C-EvalConsR ,(derive (make-evalto (make>> top-cont-env
									  (nth 2 top-cont-expr)
									  (cons (make-cont 'noenv `(cons ,v _))
										rest-conts))
								  res-val))))
		     `(derived ,judge
			       (C-Cons ,(derive (make-evalto (make=> (list 'cons (nth 1 top-cont-expr) v)
								     rest-conts)
							     res-val)))))]
		[(tagged-list? 'match top-cont-expr)
		 (if (eq? 'nil v)
		     `(derived ,judge
			       (C-MatchNil ,(derive (make-evalto (make>> top-cont-env
									 (nth 2 top-cont-expr)
									 rest-conts)
								 res-val))))
		     (let* ([v1 (nth 1 v)]
			    [v2 (nth 2 v)]
			    [bind (nth 3 top-cont-expr)]
			    [x (nth 0 bind)]
			    [y (nth 1 bind)]
			    [e2 (nth 4 top-cont-expr)])
		       `(derived ,judge
				 (C-MatchCons ,(derive (make-evalto (make>> (cons (cons y v2)
										  (cons (cons x v1)
											top-cont-env))
									    e2
									    rest-conts)
								    res-val))))))]
		[(tagged-list? 'let top-cont-expr)
		 (let* ([bind (nth 1 top-cont-expr)]
			[x (nth 0 bind)]
			[e (nth 2 top-cont-expr)])
		   `(derived ,judge
			     (C-LetBody ,(derive (make-evalto (make>> (cons (cons x v) top-cont-env) e rest-conts)
							      res-val)))))]
		[(function-app? top-cont-expr)
		 (cond [(eq? '_ (nth 0 top-cont-expr))
			`(derived ,judge
				  (C-EvalArg ,(derive (make-evalto (make>> top-cont-env
									   (nth 1 top-cont-expr)
									   (cons (make-cont 'noenv `(,v _))
										 rest-conts))
								   res-val))))]
		       [(eq? '_ (nth 1 top-cont-expr))
			(let ([fn-val (nth 0 top-cont-expr)])
			  (cond [(tagged-list? 'close fn-val)
				 (let ([env (nth 1 fn-val)]
				       [x (nth 2 fn-val)]
				       [e (nth 3 fn-val)])
				   `(derived ,judge
					     (C-EvalFun ,(derive (make-evalto (make>> (cons (cons x v) env)
										      e
										      rest-conts)
									      res-val)))))]
				[(tagged-list? 'rec fn-val)
				 (let* ([env (nth 1 fn-val)]
					[x (nth 2 fn-val)]
					[fn-expr (nth 3 fn-val)]
					[y (nth 1 fn-expr)]
					[e (nth 2 fn-expr)])
				   `(derived ,judge
					     (C-EvalFunR ,(derive (make-evalto (make>> (cons (cons y v)
											     (cons (cons x fn-val)
												   env))
										       e
										       rest-conts)
									       res-val)))))]
				[(tagged-list? 'cont fn-val)
				 (let* ([conts (nth 1 fn-val)])
				   `(derived ,judge
					     (C-EvalFunC ,(derive (make-evalto (make=> v conts)
									       res-val)))))]))])]
		[else
		 `(derived ,judge ?)])))))

(define (derive-evalto judge)
  (let ([expr (nth 1 judge)])
    (cond ([tagged-list? '=> expr]
	   (derive=> judge))
	  ([tagged-list? '>> expr]
	   (derive>> judge)))))

(define (derive judge)
  (cond ((tagged-list? 'evalto judge)
	 (derive-evalto judge))
	((tagged-list? 'plus judge)
	 `(derived ,judge (B-Plus)))
	((tagged-list? 'minus judge)
	 `(derived ,judge (B-Minus)))
	((tagged-list? 'times judge)
	 `(derived ,judge (B-Times)))
	((tagged-list? 'less judge)
	 `(derived ,judge (B-Lt)))))

;; printer
(define (print-expr e)
  (cond ((or (tagged-list? '* e) (tagged-list? '+ e) (tagged-list? '- e) (tagged-list? '< e))
	 (let ((e1 (nth 1 e))
	       (e2 (nth 2 e)))
	   (display "(")
	   (print-expr e1)
	   (display " ")
	   (display (car e))
	   (display " ")
	   (print-expr e2)
	   (display ")")))
	((tagged-list? 'if e)
	 (let ((e1 (nth 1 e))
	       (e2 (nth 2 e))
	       (e3 (nth 3 e)))
	   (display "(if ")
	   (print-expr e1)
	   (display " then ")
	   (print-expr e2)
	   (display " else ")
	   (print-expr e3)
	   (display ")")))
	((any-of-tagged-list? e 'let 'letrec)
	 (let* ((bind (nth 1 e))
		(x (nth 0 bind))
		(e1 (nth 1 bind))
		(e2 (nth 2 e)))
	   (if (tagged-list? 'let e)
	       (display "(let ")
	       (display "(let rec "))
	   (display x)
	   (display " = ")
	   (print-expr e1)
	   (display " in ")
	   (print-expr e2)
	   (display ")")))
	((tagged-list? 'letcc e)
	 (let ([x (nth 1 e)]
	       [e (nth 2 e)])
	   (display "(letcc ")
	   (display x)
	   (display " in ")
	   (print-expr e)
	   (display ")")))
	((tagged-list? 'fun e)
	 (display "fun ")
	 (display (nth 1 e))
	 (display " -> ")
	 (print-expr (nth 2 e))
	 (display ""))
	((tagged-list? 'cons e)
	 (display "((")
	 (print-expr (nth 1 e))
	 (display ") :: (")
	 (print-expr (nth 2 e))
	 (display "))"))
	((tagged-list? 'close e)
	 (display "(")
	 (print-env (nth 1 e))
	 (display ")")
	 (display "[")
	 (print-expr (list 'fun (nth 2 e) (nth 3 e)))
	 (display "]"))
	((tagged-list? 'rec e)
	 (let ((env (nth 1 e))
	       (name (nth 2 e))
	       (fn (nth 3 e)))
	   (display "(")
	   (print-env env)
	   (display ")[rec ")
	   (display name)
	   (display " = ")
	   (print-expr fn)
	   (display "]")))
	((tagged-list? 'match e)
	 (display "(match ")
	 (print-expr (nth 1 e))
	 (display " with [] -> ")
	 (print-expr (nth 2 e))
	 (display " | ")
	 (display (nth 0 (nth 3 e)))
	 (display "::")
	 (display (nth 1 (nth 3 e)))
	 (display " -> ")
	 (print-expr (nth 4 e))
	 (display ")"))
	((equal? 'nil e)
	 (display "[]"))
	[(tagged-list? 'cont e)
	 (display "[")
	 (print-conts (nth 1 e))
	 (display "]")]
	((function-app? e)
	 (display "(")
	 (print-expr (nth 0 e))
	 (display " ")
	 (print-expr (nth 1 e))
	 (display ")"))
	(else
	 (if (and (integer? e) (> 0 e))
	     (begin (display "(") (display e) (display ")"))
	     (display e)))))

;; TODO:
(define (print-expr-noparen e)
  (cond ((or (tagged-list? '* e) (tagged-list? '+ e) (tagged-list? '- e) (tagged-list? '< e))
	 (let ((e1 (nth 1 e))
	       (e2 (nth 2 e)))
	   (print-expr e1)
	   (display " ")
	   (display (car e))
	   (display " ")
	   (print-expr e2)))
	((tagged-list? 'if e)
	 (let ((e1 (nth 1 e))
	       (e2 (nth 2 e))
	       (e3 (nth 3 e)))
	   (display "if ")
	   (print-expr e1)
	   (display " then ")
	   (print-expr e2)
	   (display " else ")
	   (print-expr e3)))
	((any-of-tagged-list? e 'let 'letrec)
	 (let* ((bind (nth 1 e))
		(x (nth 0 bind))
		(e1 (nth 1 bind))
		(e2 (nth 2 e)))
	   (if (tagged-list? 'let e)
	       (display "let ")
	       (display "let rec "))
	   (display x)
	   (display " = ")
	   (print-expr e1)
	   (display " in ")
	   (print-expr e2)))
	((tagged-list? 'fun e)
	 (display "fun ")
	 (display (nth 1 e))
	 (display " -> ")
	 (print-expr (nth 2 e)))
	((equal? 'nil e)
	 (display "[]"))
	((tagged-list? 'letcc e)
	 (let ([x (nth 1 e)]
	       [e (nth 2 e)])
	   (display "letcc ")
	   (display x)
	   (display " in ")
	   (print-expr e)))
	((tagged-list? 'cons e)
	 (print-expr (nth 1 e))
	 (display " :: ")
	 (print-expr (nth 2 e)))
	((tagged-list? 'match e)
	 (display "match ")
	 (print-expr (nth 1 e))
	 (display " with [] -> ")
	 (print-expr (nth 2 e))
	 (display " | ")
	 (display (nth 0 (nth 3 e)))
	 (display "::")
	 (display (nth 1 (nth 3 e)))
	 (display " -> ")
	 (print-expr (nth 4 e)))
	((function-app? e)
	 (print-expr (nth 0 e))
	 (display " ")
	 (print-expr (nth 1 e)))
	(else ;; CAUTION: default case
	 (print-expr e))))


;; TODO: adopt for function, continuation
;; TODO: not neccessary???
(define (print-val v)
  (cond [(tagged-list? 'close v)
	 (display "(")
	 (print-env (nth 1 v))
	 (display ")[")
	 (print-expr (list 'fun (nth 2 v) (nth 3 v)))
	 (display "]")]
	[(tagged-list? 'rec v)
	 (display "(")
	 (print-env (nth 1 v))
	 (display ")[rec ")
	 (display (nth 2 v))
	 (display " = ")
	 (print-expr (nth 3 v))
	 (display "]")]
	[(tagged-list? 'cont v)
	 (display "[")
	 (print-conts (nth 1 v))
	 (display "]")]
	[(eq? 'nil v)
	 (display "[]")]
	[(tagged-list? 'cons v)
	 (print-val (nth 1 v))
	 (display " :: ")
	 (print-val (nth 2 v))]
	[else
	 (if (and (integer? v) (> 0 v))
	     (begin (display "(") (display v) (display ")"))
	     (display v))]))

(define (print-env env)
  (if (null? env)
      '()
      (let loop ((env (reverse env)))
	  (let* ((vv (car env))
		 (var (car vv))
		 (val (cdr vv)))
	    (display var)
	    (display " = ")
	    (print-val val)
	    (if (null? (cdr env))
		'()
		(begin
		  (display " , ")
		  (loop (cdr env))))))))

(define (print-cont cont)
  (if (eq? cont '_)
      (display "_")
      (let* ([env (car cont)]
	     [expr (cdr cont)])
	(display "{")
	(when (not (eq? env 'noenv))
	      (print-env env)
	      (display " |- "))
	(print-expr-noparen expr)
	(display "}"))))

(define (print-conts conts)
  (if (null? (cdr conts))
      (print-cont (car conts))
      (begin (print-cont (car conts))
	     (display " >> ")
	     (print-conts (cdr conts)))))

(define (print=> expr)
  (print-val (nth 1 expr))
  (display " => ")
  (print-conts (nth 2 expr)))

(define (print>> expr)
  (print-env (nth 1 expr))
  (display " |- ")
  (print-expr (nth 2 expr))
  (display " >> ")
  (print-conts (nth 3 expr)))

(define (print-judge judge)
  (cond [(tagged-list? 'evalto judge)
	 (if (tagged-list? '=> (nth 1 judge))
	     (print=> (nth 1 judge))
	     (print>> (nth 1 judge)))
	 (display " evalto ")
	 (print-val (nth 2 judge))]
	[else
	 (display (nth 1 judge))
	 (display " ")
	 (display (if (eq? (nth 0 judge) 'less)
		      "less than"
		      (nth 0 judge)))
	 (display " ")
	 (display (nth 2 judge))
	 (display " is ")
	 (display (nth 3 judge))]))

(define (make-answer judge)
  (println judge)
  (println "========================")
  (print-derived print-judge (derive judge))
  (newline)
  (println "========================"))

(define q130 (make-evalto (make>> '()
				  '(let (x (+ 1 2))
				     (* x 4))
				  '(_))
			  12))

(define q131 (make-evalto (make>> '()
				  '(let (add1 (fun x (+ x 1)))
				     (add1 3))
				  '(_))
			  4))

(define q132 (make-evalto (make>> '()
				  '(letrec (fact (fun n (if (< n 2) 1 (* n (fact (- n 1))))))
				     (fact 3))
				  '(_))
			  6))

(define q133 (make-evalto (make>> '((k . (cont ((noenv . (+ 3 _)) _))))
				  '(+ 1 (k 2))
				  '(_))
			  5))

(define q134 (make-evalto (make>> '()
				  '(+ 3
				      (letcc k
					     (+ 1 (k 2))))
				  '(_))
			  5))

(define q135 (make-evalto (make>> '()
				  '(letrec (fact (fun n (if (< n 2) 1 (* n (fact (- n 1))))))
				     (+ 3 (letcc k (+ (+ 1 (k 2)) (fact 100)))))
				  '(_))
			  5))

(define q136 (make-evalto (make>> '()
				  '(let (sm (fun f (+ (f 3) (f 4))))
				     (letcc k (sm k)))
				  '(_))
			  3))

(define q137 (make-evalto (make>> '()
				  '(let (f (fun x
						(fun k1
						     (fun k2
							  (if (< x 0)
							      (k1 x)
							      (k2 x))))))
				     (+ 1 (letcc k1 (+ 2 (letcc k2 (((f -2) k1) k2))))))
				  '(_))
			  -1))

(define q138 (make-evalto (make>> '()
				  '(let (f (fun x
						(fun k1
						     (fun k2
							  (if (< x 0)
							      (k1 x)
							      (k2 x))))))
				     (+ 1 (letcc k1 (+ 2 (letcc k2 (((f 2) k1) k2))))))
				  '(_))
			  5))

(define q139 (make-evalto (make>> '()
				  '(letrec (findneg (fun l (match l
								  false
								  (x l) (if (< x 0) true (findneg l)))))
				     (findneg (cons 1 (cons 2 (cons -3 (cons 4 nil))))))
				  '(_))
			  'true))

(define q140 (make-evalto (make>> '()
				  '(let (findneg (fun l (letcc k
							       (letrec (aux (fun l (match l
											  false
											  (x l) (if (< x 0) (k true) (aux l)))))
								 (aux l)))))
				     (findneg (cons 1 (cons 2 (cons -3 (cons 4 nil))))))
				  '(_))
			  'true))

(map make-answer (list q130 q131 q132 q133 q134 q135 q136 q137 q138 q139 q140))
