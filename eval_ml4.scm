(load "./utils.scm")

;; judge format
'(evalto env expr val)
'(less 1 2 true)
;; e
'(if (< 1 2) 1 3)
'(let (x . e) (+ x 3))

;; fun
'(fun var expr)
;; closure (fun evalto closure)
'(close env var expr)

;; rec
'(letrec (fact . (fun n (if (< n 2) 1 (* n (fact (- n 1))))))
   (fact 6))
;; rec closure
'(rec env fact (fun n (if (< n 2) 1 (* n (fact (- n 1))))))

;; :: = cons
;; (1 :: (2 :: []))
'(cons 1 (cons 2 nil))

;; match
;; match e1 with [] -> e2 | x::y -> e3
'(match e1 e2 (x y) e3)

(define (make-rec env name fn)
  (list 'rec env name fn))
(define (rec-env rec)
  (nth 1 rec))
(define (rec-name rec)
  (nth 2 rec))
(define (rec-fn rec)
  (nth 3 rec))

(define (variable? expr)
  (and (not (any-of? expr 'true 'false 'nil)) (symbol? expr)))

(define (make-evalto env expr val)
  (list 'evalto env expr val))

(define (eval-arith expr env)
  (cond ((any-of-tagged-list? expr '+ '* '-)
	 (let ((v1 (eval-arith (nth 1 expr) env))
	       (v2 (eval-arith (nth 2 expr) env)))
	   ((symbol->proc (car expr)) v1 v2)))
	((tagged-list? '< expr)
	 (let ((v1 (eval-arith (nth 1 expr) env))
	       (v2 (eval-arith (nth 2 expr) env)))
	   (if (< v1 v2)
	       'true
	       'false)))
	((tagged-list? 'if expr)
	 (let ((b (eval-arith (nth 1 expr) env)))
	   (if (eq? b 'true)
	       (eval-arith (nth 2 expr) env)
	       (eval-arith (nth 3 expr) env))))
	((tagged-list? 'let expr)
	 (let* ((bind (nth 1 expr))
		(x (car bind))
		(e1 (cdr bind))
		(v (eval-arith e1 env))
		(e2 (nth 2 expr)))
	   (eval-arith e2
		       (cons (cons x v) env))))
	((tagged-list? 'letrec expr)
	 (let* ((bind (nth 1 expr))
		(x (car bind))
		(efn (cdr bind))
		(e2 (nth 2 expr)))
	   (eval-arith e2
		       (cons (cons x (make-rec env x efn))
			     env))))
	((tagged-list? 'fun expr)
	 `(close ,env ,(nth 1 expr) ,(nth 2 expr)))
	((tagged-list? 'cons expr)
	 `(cons ,(eval-arith (nth 1 expr) env) ,(eval-arith (nth 2 expr) env)))
	((tagged-list? 'match expr)
	 (let* ((e1 (nth 1 expr))
		(e2 (nth 2 expr))
		(bind (nth 3 expr))
		(e3 (nth 4 expr))
		(x (nth 0 bind))
		(y (nth 1 bind))
		(v1 (eval-arith e1 env)))
	   (if (eq? v1 'nil)
	       (eval-arith e2 env)
	       (let ((newenv (cons (cons y (nth 2 v1))
				   (cons (cons x (nth 1 v1))
					 env))))
		 (eval-arith e3 newenv)))))
	((variable? expr)
	 (get-assoc expr env))
	((any-of? expr 'true 'false)
	 expr)
	((integer? expr) expr)
	((equal? 'nil expr) 'nil)
	(else
	 (let* ((fn (car expr))
		(arg (cadr expr)) ;; TODO: check if arg is one.
		(close (eval-arith fn env))
		(arg-val (eval-arith arg env)))
	   (cond ((tagged-list? 'close close)
		  (eval-arith
		   (nth 3 close)
		   (cons (cons (nth 2 close) arg-val)
			 (nth 1 close))))
		 ((tagged-list? 'rec close)
		  (let* ((rec-name (rec-name close))
			 (rec-env (rec-env close))
			 (rec-fn (rec-fn close))
			 (rec-fn-var (nth 1 rec-fn))
			 (next (nth 2 rec-fn))
			 (newenv (cons
				  (cons rec-fn-var arg-val)
				  (cons
				   (cons rec-name close)
				   rec-env))))
		    (eval-arith next newenv)))
		 (else (error "eval-arith: car of expr is not closue. expr=" expr)))))))

(define (derive-evalto judge)
  (let* ((env (nth 1 judge))
	 (expr (nth 2 judge))
	 (val (nth 3 judge)))
    (cond ((variable? expr)
	   `(derived
	     ,judge
	     (E-Var)))
	  ((any-of-tagged-list? expr '+ '- '*)
	   (let* ((op (nth 0 expr))
		  (e1 (nth 1 expr))
		  (e2 (nth 2 expr))
		  (i1 (eval-arith e1 env))
		  (i2 (eval-arith e2 env))
		  (i ((symbol->proc op) i1 i2)))
	     `(derived
	       ,judge
	       (,(get-assoc op '((+ . E-Plus) (* . E-Times) (- . E-Minus)))
		,(derive (make-evalto env e1 i1))
		,(derive (make-evalto env e2 i2))
		,(derive (list (get-assoc op '((+ . plus) (* . times) (- . minus)))
			       i1
			       i2
			       i))))))
	  ((tagged-list? '< expr)
	   (let* ((e1 (nth 1 expr))
		  (e2 (nth 2 expr))
		  (i1 (eval-arith e1 env))
		  (i2 (eval-arith e2 env)))
	     `(derived
	       ,judge
	       (E-Lt
		,(derive (make-evalto env e1 i1))
		,(derive (make-evalto env e2 i2))
		,(derive (list 'less i1 i2 (eval-arith (list '< i1 i2) env)))))))
	  ((tagged-list? 'if expr)
	   (let* ((e1 (nth 1 expr))
		  (e2 (nth 2 expr))
		  (e3 (nth 3 expr))
		  (b (eval-arith e1 env))
		  (v2 (eval-arith e2 env))
		  (v3 (eval-arith e3 env)))
	     (if (eq? b 'true)
		 `(derived ,judge
			   (E-IfT
			    ,(derive (make-evalto env e1 b))
			    ,(derive (make-evalto env e2 v2))))
		 `(derived ,judge
			   (E-IfF
			    ,(derive (make-evalto env e1 b))
			    ,(derive (make-evalto env e3 v3)))))))
	  ((tagged-list? 'let expr)
	   (let* ((bind (nth 1 expr))
		  (x (car bind))
		  (e1 (cdr bind))
		  (e2 (nth 2 expr))
		  (v1 (eval-arith e1 env))
		  (newenv (cons (cons x v1) env))
		  (v2 (eval-arith e2 newenv)))
	     `(derived ,judge
		       (E-Let
			,(derive (make-evalto env e1 v1))
			,(derive (make-evalto newenv e2 v2))))))
	  ((tagged-list? 'letrec expr)
	   (let* ((bind (nth 1 expr))
		  (x (car bind))
		  (efn (cdr bind))
		  (e2 (nth 2 expr))
		  (newenv (cons (cons x (make-rec env x efn)) env)))
	     `(devied ,judge
		      (E-LetRec
		       ,(derive (make-evalto newenv e2 val))))))
	  ((tagged-list? 'fun expr)
	   `(derived ,judge
		     (E-Fun)))
	  ((tagged-list? 'cons expr)
	   (let* ((car-expr (nth 1 expr))
		  (cdr-expr (nth 2 expr))
		  (car-v (eval-arith car-expr env))
		  (cdr-v (eval-arith cdr-expr env)))
	     `(derived ,judge
		       (E-Cons
			,(derive (make-evalto env car-expr car-v))
			,(derive (make-evalto env cdr-expr cdr-v))))))
	  ((tagged-list? 'match expr)
	   (let* ((e1 (nth 1 expr))
		  (e2 (nth 2 expr))
		  (bind (nth 3 expr))
		  (e3 (nth 4 expr))
		  (x (nth 0 bind))
		  (y (nth 1 bind))
		  (v1 (eval-arith e1 env)))
	     (if (eq? v1 'nil)
		 (let ((v2 (eval-arith e2 env)))
		   `(derived ,judge
			     (E-MatchNil
			      ,(derive (make-evalto env e1 v1))
			      ,(derive (make-evalto env e2 v2)))))
		 (let* ((newenv (cons (cons y (nth 2 v1))
				      (cons (cons x (nth 1 v1))
					    env)))
			(v3 (eval-arith e3 newenv)))
		   `(derived ,judge
			     (E-MatchCons
			      ,(derive (make-evalto env e1 v1))
			      ,(derive (make-evalto newenv e3 v3))))))))
	  ((and (list? expr) (= 2 (length expr)))
	   (let* ((fn (car expr))
		  (arg (cadr expr))
		  (fn-val (eval-arith fn env))
		  (arg-val (eval-arith arg env)))
	     (if (tagged-list? 'close fn-val)
		 (let ((fn-env (nth 1 fn-val))
		       (fn-var (nth 2 fn-val))
		       (fn-expr (nth 3 fn-val)))
		   `(derived ,judge
			     (E-App
			      ,(derive (make-evalto env fn fn-val))
			      ,(derive (make-evalto env arg arg-val))
			      ,(derive (make-evalto (cons (cons fn-var arg-val) fn-env)
						    fn-expr
						    val)))))
		 ;; rec
		 (let* ((rec-name (rec-name fn-val))
			(rec-env (rec-env fn-val))
			(rec-fn (rec-fn fn-val))
			(rec-fn-var (nth 1 rec-fn))
			(next (nth 2 rec-fn))
			(newenv (cons
				 (cons rec-fn-var arg-val)
				 (cons
				  (cons rec-name fn-val)
				  rec-env))))
		   `(derived ,judge
			     (E-AppRec
			      ,(derive (make-evalto env fn fn-val))
			      ,(derive (make-evalto env arg arg-val))
			      ,(derive (make-evalto newenv next val))))))))
	  ((integer? expr)
	   `(derived ,judge (E-Int)))
	  ((any-of? expr 'true 'false)
	   `(derived ,judge (E-Bool)))
	  ((equal? 'nil expr)
	   `(derived ,judge (E-Nil)))
	  (else (error "derive-evalto: unknown " judge)))))

(define (derive judge)
  (cond ((tagged-list? 'evalto judge)
	 (derive-evalto judge))
	((tagged-list? 'plus judge)
	 `(derived ,judge (B-Plus)))
	((tagged-list? 'minus judge)
	 `(derived ,judge (B-Minus)))
	((tagged-list? 'times judge)
	 `(derived ,judge (B-Times)))
	((tagged-list? 'less judge)
	 `(derived ,judge (B-Lt)))
	(else
	 (error "derive: unknown expression:" judge))))

;; TODO: mutual recursion print-env <-> print-arith is ok???
(define (print-env env)
  (if (null? env)
      '()
      (let loop ((env env))
	  (let* ((vv (car env))
		 (var (car vv))
		 (val (cdr vv)))
	    (display var)
	    (display " = ")
	    (print-arith val)
	    (if (null? (cdr env))
		'()
		(begin
		  (display " , ")
		  (print-env (cdr env))))))))

;; TODO: divide into print-arith and print-value
;; close is not expression but value.
(define (print-arith e)
  (cond ((or (tagged-list? '* e) (tagged-list? '+ e) (tagged-list? '- e) (tagged-list? '< e))
	 (let ((e1 (nth 1 e))
	       (e2 (nth 2 e)))
	   (display "(")
	   (print-arith e1)
	   (display " ")
	   (display (car e))
	   (display " ")
	   (print-arith e2)
	   (display ")")))
	((tagged-list? 'if e)
	 (let ((e1 (nth 1 e))
	       (e2 (nth 2 e))
	       (e3 (nth 3 e)))
	   (display "(if ")
	   (print-arith e1)
	   (display " then ")
	   (print-arith e2)
	   (display " else ")
	   (print-arith e3)
	   (display ")")))
	((any-of-tagged-list? e 'let 'letrec)
	 (let* ((bind (nth 1 e))
		(x (car bind))
		(e1 (cdr bind))
		(e2 (nth 2 e)))
	   (if (tagged-list? 'let e)
	       (display "(let ")
	       (display "(let rec "))
	   (display x)
	   (display " = ")
	   (print-arith e1)
	   (display " in ")
	   (print-arith e2)
	   (display ")")))
	((tagged-list? 'fun e)
	 (display "fun ")
	 (display (nth 1 e))
	 (display " -> ")
	 (print-arith (nth 2 e))
	 (display ""))
	((tagged-list? 'cons e)
	 (display "((")
	 (print-arith (nth 1 e))
	 (display ") :: (")
	 (print-arith (nth 2 e))
	 (display "))"))
	((tagged-list? 'close e)
	 (display "(")
	 (print-env (reverse (nth 1 e)))
	 (display ")")
	 (display "[")
	 (print-arith (list 'fun (nth 2 e) (nth 3 e)))
	 (display "]"))
	((tagged-list? 'rec e)
	 (let ((env (rec-env e))
	       (name (rec-name e))
	       (fn (rec-fn e)))
	   (display "(")
	   (print-env (reverse env))
	   (display ")[rec ")
	   (display name)
	   (display " = ")
	   (print-arith fn)
	   (display "]")))
	((tagged-list? 'match e)
	 (display "(match ")
	 (print-arith (nth 1 e))
	 (display " with [] -> ")
	 (print-arith (nth 2 e))
	 (display " | ")
	 (display (nth 0 (nth 3 e)))
	 (display "::")
	 (display (nth 1 (nth 3 e)))
	 (display " -> ")
	 (print-arith (nth 4 e))
	 (display ")"))
	((list? e)
	 (display "(")
	 (print-arith (car e))
	 (display " (")
	 (print-arith (cadr e))
	 (display "))"))
	((equal? 'nil e)
	 (display "[]"))
	(else
	 (display e))))

(define (print-judge judge)
  (cond ((tagged-list? 'evalto judge)
	 (let ((env (nth 1 judge))
	       (expr (nth 2 judge))
	       (val (nth 3 judge)))
	   (print-env (reverse env))
	   (display " |- ")
	   (print-arith expr)
	   (display " evalto ")
	   (print-arith val)))
	(else (let ((op (car judge))
		    (n1 (cadr judge))
		    (n2 (caddr judge))
		    (n3 (cadddr judge)))
		(display n1)
		(display " ")
		(if (eq? op 'less)
		    (display " less than ")
		    (display op))
		(display " ")
		(display n2)
		(display " is ")
		(display n3)))))

(define (print-derived derived)
  (let ((judge (cadr derived))
	(reason (caddr derived)))
    (print-judge judge)
    (display " by ")
    (let ((rule (car reason))
	  (children-derived (cdr reason)))
      (display rule)
      (display " {")
      (print-children-derived children-derived)
      (display "}"))))

(define (print-children-derived children)
  (if (null? children)
      "{}"
      (let loop ((children children))
	(if (null? (cdr children))
	    (print-derived (car children))
	    (begin (print-derived (car children))
		   (display ";")
		   (newline)
		   (print-children-derived (cdr children)))))))

(define (make-answer judge)
  (print-derived (derive judge))
  (newline))

(print-judge '(evalto () (+ 1 2) 3))
(newline)
(print-judge '(evalto ((x . 1) (y . 2)) (+ x y) 3))
(newline)

(eval-arith 'x '((x . 1) (y . 2)))

(define (make-input env expr val)
  `(evalto ,(reverse env) ,expr ,val))


(make-answer (make-input '() 'nil 'nil))

(define q70 (make-input '() '(cons (+ 1 2) (cons (+ 3 4) nil)) '(cons 3 (cons 7 nil))))

(define take-car-prog
  '(let (f . (fun x (match x 0 (a b) a)))
			   (+ (+ (f (cons 4 nil))
				 (f nil))
			      (f (cons 1 (cons 2 (cons 3 nil)))))))

(define q71 (make-input '()
			take-car-prog
			5))

(define q72 (make-input '()
			'(letrec (f . (fun x (if (< x 1) nil (cons x (f (- x 1))))))
			   (f 3))
			'(cons 3 (cons 2 (cons 1 nil)))))

(define q73 (make-input '()
			'(letrec (length . (fun l (match l 0 (x y) (+ 1 (length y)))))
			   (length (cons 1 (cons 2 (cons 3 nil)))))
			3))

(define q74 (make-input '()
			'(letrec (length . (fun l (match l 0 (x y) (+ 1 (length y)))))
			   (length (cons (cons 1 (cons 2 nil))
					 (cons (cons 3 (cons 4 (cons 5 nil)))
					       nil))))
			2))

(define append-prog '(letrec (append . (fun l1 (fun l2
						    (match l1 l2 (x y) (cons x ((append y) l2))))))
			   ((append (cons 1 (cons 2 nil)))
			    (cons 3 (cons 4 (cons 5 nil))))))

(define q75 (make-input '()
			append-prog
			'(cons 1 (cons 2 (cons 3 (cons 4 (cons 5 nil)))))))

(define q76 (make-input '()
			'(letrec (apply . (fun l (fun x (match l x (f l) (f ((apply l) x))))))
			   ((apply (cons (fun x (* x x)) (cons (fun y (+ y 3)) nil)))
			    4))
			49))

(define q77 (make-input '()
			'(letrec (apply . (fun l (fun x (match l x (f l) ((apply l) (f x))))))
			   ((apply (cons (fun x (* x x)) (cons (fun y (+ y 3)) nil)))
			    4))
			19))

(map (lambda (d)
       (println "========================================")
       (make-answer d)) (list q70 q71 q72 q73 q74 q75 q76 q77))

;; debug this to pass q76
(make-answer (make-input '()
			 '(cons (fun x (* x x))
				nil)
			 '(cons (close () x (* x x)) nil)))
