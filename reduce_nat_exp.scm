(load "./utils.scm")

(define (no-redux? expr)
  (not (any-of-tagged-list? expr '+ '*)))

;; ---> evaluator
;; eval rightmost redux
(define (step-rightmost expr)
  (if (any-of-tagged-list? expr '+ '*)
      (if (and (no-redux? (nth 1 expr)) (no-redux?  (nth 2 expr)))
	  (let ((n1 (int-from-peano (nth 1 expr)))
		(n2 (int-from-peano (nth 2 expr))))
	    (make-peano-nat ((symbol->proc (car expr)) n1 n2)))
	  `(,(car expr) ,(nth 1 expr) ,(step-rightmost (nth 2 expr))))
      (error "step: no more redux. expr=" expr)))

;; judge is like (plus (S Z) (Z) (S Z))
;; assume that judge is true under Nat system.
;; so no error handling for now
(define (derive judge)
  (cond ((tagged-list? '-*-> judge)
	 `(derived ,judge
		   (MR-One
		    ,(derive (list '---> (nth 1 judge) (nth 2 judge))))))
	((tagged-list? '---> judge)
	 (let* ((e (nth 1 judge))
		(v (nth 2 judge))
		(e1 (nth 1 e))
		(e2 (nth 2 e))
		(op (car e)))
	   (cond ((and (no-redux? e1) (no-redux? e2))
		  `(derived ,judge
			    (,(get-assoc op '((+ . R-Plus) (* . R-Times)))
			     ,(derive (list (get-assoc op '((+ . plus) (* . times))) e1 e2 v)))))
		 (else (error "derive: not supported yet. " judge)))))
	((tagged-list? 'plus judge)
	 (let ((n1 (cadr judge))
	       (n2 (caddr judge))
	       (n3 (cadddr judge)))
	   (if (and (tagged-list? 'S n1))
	       `(derived 	       ;; apply P-Succ
		 ,judge
		 (P-Succ
		  ,(derive (list 'plus (cdr n1) n2 (cdr n3)))))
	       `(derived
		 ,judge
		 (P-Zero)))))
	((tagged-list? 'times judge)
	 (let* ((sn1 (nth 1 judge))
		(n2 (nth 2 judge))
		(n4 (nth 3 judge)))
	   (if (and (tagged-list? 'S sn1))
	       (let* ((n1 (cdr sn1))
		      (n3 (make-peano-nat (* (int-from-peano n1) (int-from-peano n2))))) ;; TODO: n3 = n1 * n2
		 `(derived 	       ;; apply T-Succ
		   ,judge
		   (T-Succ
		    ,(derive (list 'times n1 n2 n3))
		    ,(derive (list 'plus n2 n3 n4)))))
	       `(derived
		 ,judge
		 (T-Zero)))))
	(else (error "cannot derive:" judge))))

(define (print-arith e)
  (if (or (tagged-list? '* e) (tagged-list? '+ e))
      (let ((e1 (nth 1 e))
	    (e2 (nth 2 e)))
	(display "(")
	(print-arith e1)
	(display " ")
	(display (car e))
	(display " ")
	(print-arith e2)
	(display ")"))
      (print-nat e)))

(define (print-judge judge)
  (cond ((any-of-tagged-list? judge '---> '-*->)
	 (print-arith (nth 1 judge))
	 (display " ")
	 (display (nth 0 judge))
	 (display " ")
	 (print-arith (nth 2 judge)))
	(else
	 (let ((op (car judge))
	       (n1 (cadr judge))
	       (n2 (caddr judge))
	       (n3 (cadddr judge)))
	   (print-nat n1)
	   (display " ")
	   (display op)
	   (display " ")
	   (print-nat n2)
	   (display " is ")
	   (print-nat n3)))))

(define (print-derived derived)
  (let ((judge (cadr derived))
	(reason (caddr derived)))
    (print-judge judge)
    (display " by ")
    (let ((rule (car reason))
	  (children-derived (cdr reason)))
      (display rule)
      (display " {")
      (print-children-derived children-derived)
      (display "}"))))

(define (print-children-derived children)
  (if (null? children)
      "{}"
      (let loop ((children children))
	(if (null? (cdr children))
	    (print-derived (car children))
	    (begin (print-derived (car children))
		   (display ";")
		   (print-children-derived (cdr children)))))))

;; TODO: add parser that parse 'Z plus Z is Z'
(define (make-answer judge)
  (print-derived (derive judge))
  (newline))

(define q21 '(-*-> (+ (Z) (S S Z)) (S S Z)))

(map make-answer (list q21))

