(load "./utils.scm")

(load "./utils.scm")

;; judge format
'(evalto env expr val)
'(less 1 2 true)
;; e
'(if (< 1 2) 1 3)
'(let (x . e) (+ x 3))

;; env
'(1 true 3 10)

(define (lookup-environment n env)
  (if (= n 1)
      (car env)
      (lookup-environment (- n 1) (cdr env))))

;; nameless varialbe
'(sharp 1)

;; fun
'(fun expr)
;; closure (fun evalto closure)
'(close env expr)

;; rec
'(letrec (fun (if (< n 2) 1 (* n (fact (- n 1)))))
   (fact 6))
;; rec closure
'(rec env fact (fun n (if (< n 2) 1 (* n (fact (- n 1))))))

(define (make-rec env fn)
  (list 'rec env fn))
(define (rec-env rec)
  (nth 1 rec))
(define (rec-fn rec)
  (nth 2 rec))

(define (variable? expr)
  (and (not (any-of? expr 'true 'false)) (symbol? expr)))

(define (make-evalto env expr val)
  (list 'evalto env expr val))

(define (eval-arith expr env)
  (cond ((any-of-tagged-list? expr '+ '* '-)
	 (let ((v1 (eval-arith (nth 1 expr) env))
	       (v2 (eval-arith (nth 2 expr) env)))
	   ((symbol->proc (car expr)) v1 v2)))
	((tagged-list? '< expr)
	 (let ((v1 (eval-arith (nth 1 expr) env))
	       (v2 (eval-arith (nth 2 expr) env)))
	   (if (< v1 v2)
	       'true
	       'false)))
	((tagged-list? 'if expr)
	 (let ((b (eval-arith (nth 1 expr) env)))
	   (if (eq? b 'true)
	       (eval-arith (nth 2 expr) env)
	       (eval-arith (nth 3 expr) env))))
	((tagged-list? 'let expr)
	 (let* ((e1 (nth 1 expr))
		(v (eval-arith e1 env))
		(e2 (nth 2 expr)))
	   (eval-arith e2
		       (cons v env))))
	((tagged-list? 'letrec expr)
	 (let* ((efn (nth 1 expr))
		(e2 (nth 2 expr)))
	   (eval-arith e2
		       (cons (make-rec env efn)  env))))
	((tagged-list? 'fun expr)
	 `(close ,env ,(nth 1 expr)))
	((tagged-list? 'sharp expr)
	 (lookup-environment (cadr expr) env)) ;; OK???
	((any-of? expr 'true 'false)
	 expr)
	((integer? expr) expr)
	(else
	 (let* ((fn (car expr))
		(arg (cadr expr)) ;; TODO: check if arg is one.
		(close (eval-arith fn env))
		(arg-val (eval-arith arg env)))
	   (cond ((tagged-list? 'close close)
		  (eval-arith
		   (nth 2 close)
		   (cons arg-val
			 (nth 1 close))))
		 ((tagged-list? 'rec close)
		  (let* ((rec-env (rec-env close))
			 (rec-fn (rec-fn close))
			 (next (nth 1 rec-fn))
			 (newenv (cons
				  arg-val
				  (cons
				   close
				   rec-env))))
		    (eval-arith next newenv)))
		 (else (error "eval-arith: car of expr is not closue. expr=" expr)))))))

(define (derive-evalto judge)
  (let* ((env (nth 1 judge))
	 (expr (nth 2 judge))
	 (val (nth 3 judge)))
    (cond ((tagged-list? 'sharp expr)
	   `(derived
	     ,judge
	     (E-Var)))
	  ((any-of-tagged-list? expr '+ '- '*)
	   (let* ((op (nth 0 expr))
		  (e1 (nth 1 expr))
		  (e2 (nth 2 expr))
		  (i1 (eval-arith e1 env))
		  (i2 (eval-arith e2 env))
		  (i ((symbol->proc op) i1 i2)))
	     `(derived
	       ,judge
	       (,(get-assoc op '((+ . E-Plus) (* . E-Times) (- . E-Minus)))
		,(derive (make-evalto env e1 i1))
		,(derive (make-evalto env e2 i2))
		,(derive (list (get-assoc op '((+ . plus) (* . times) (- . minus)))
			       i1
			       i2
			       i))))))
	  ((tagged-list? '< expr)
	   (let* ((e1 (nth 1 expr))
		  (e2 (nth 2 expr))
		  (i1 (eval-arith e1 env))
		  (i2 (eval-arith e2 env)))
	     `(derived
	       ,judge
	       (E-Lt
		,(derive (make-evalto env e1 i1))
		,(derive (make-evalto env e2 i2))
		,(derive (list 'less i1 i2 (eval-arith (list '< i1 i2) env)))))))
	  ((tagged-list? 'if expr)
	   (let* ((e1 (nth 1 expr))
		  (e2 (nth 2 expr))
		  (e3 (nth 3 expr))
		  (b (eval-arith e1 env))
		  (v2 (eval-arith e2 env))
		  (v3 (eval-arith e3 env)))
	     (if (eq? b 'true)
		 `(derived ,judge
			   (E-IfT
			    ,(derive (make-evalto env e1 b))
			    ,(derive (make-evalto env e2 v2))))
		 `(derived ,judge
			   (E-IfF
			    ,(derive (make-evalto env e1 b))
			    ,(derive (make-evalto env e3 v3)))))))
	  ((tagged-list? 'let expr)
	   (let* ((e1 (nth 1 expr))
		  (e2 (nth 2 expr))
		  (v1 (eval-arith e1 env))
		  (newenv (cons v1 env))
		  (v2 (eval-arith e2 newenv)))
	     `(derived ,judge
		       (E-Let
			,(derive (make-evalto env e1 v1))
			,(derive (make-evalto newenv e2 v2))))))
	  ((tagged-list? 'letrec expr)
	   (let* ((efn (nth 1 expr))
		  (e2 (nth 2 expr))
		  (newenv (cons (make-rec env efn) env)))
	     `(devied ,judge
		      (E-LetRec
		       ,(derive (make-evalto newenv e2 val))))))
	  ((tagged-list? 'fun expr)
	   `(derived ,judge
		     (E-Fun)))
	  ((and (list? expr) (= 2 (length expr)))
	   (let* ((fn (car expr))
		  (arg (cadr expr))
		  (fn-val (eval-arith fn env))
		  (arg-val (eval-arith arg env)))
	     (if (tagged-list? 'close fn-val)
		 (let ((fn-env (nth 1 fn-val))
		       (fn-expr (nth 2 fn-val)))
		   `(derived ,judge
			     (E-App
			      ,(derive (make-evalto env fn fn-val))
			      ,(derive (make-evalto env arg arg-val))
			      ,(derive (make-evalto (cons arg-val fn-env)
						    fn-expr
						    val)))))
		 ;; rec
		 (let* ((rec-env (rec-env fn-val))
			(rec-fn (rec-fn fn-val))
			(next (nth 1 rec-fn))
			(newenv (cons
				 arg-val
				 (cons
				  fn-val
				  rec-env))))
		   `(derived ,judge
			     (E-AppRec
			      ,(derive (make-evalto env fn fn-val))
			      ,(derive (make-evalto env arg arg-val))
			      ,(derive (make-evalto newenv next val))))))))
	  ((integer? expr)
	   `(derived ,judge (E-Int)))
	  ((any-of? expr 'true 'false)
	   `(derived ,judge (E-Bool)))
	  (else (error "derive-evalto: unknown " judge)))))

(define (derive judge)
  (cond ((tagged-list? 'evalto judge)
	 (derive-evalto judge))
	((tagged-list? 'plus judge)
	 `(derived ,judge (B-Plus)))
	((tagged-list? 'minus judge)
	 `(derived ,judge (B-Minus)))
	((tagged-list? 'times judge)
	 `(derived ,judge (B-Times)))
	((tagged-list? 'less judge)
	 `(derived ,judge (B-Lt)))
	(else
	 (error "derive: unknown expression:" judge))))

;; TODO: mutual recursion print-env <-> print-arith is ok???
(define (print-env env)
  (if (null? env)
      '()
      (let loop ((env env))
	  (let* ((v (car env)))
	    (print-arith v)
	    (if (null? (cdr env))
		'()
		(begin
		  (display " , ")
		  (print-env (cdr env))))))))

;; TODO: divide into print-arith and print-value
;; close is not expression but value.
(define (print-arith e)
  (cond ((or (tagged-list? '* e) (tagged-list? '+ e) (tagged-list? '- e) (tagged-list? '< e))
	 (let ((e1 (nth 1 e))
	       (e2 (nth 2 e)))
	   (display "(")
	   (print-arith e1)
	   (display " ")
	   (display (car e))
	   (display " ")
	   (print-arith e2)
	   (display ")")))
	((tagged-list? 'if e)
	 (let ((e1 (nth 1 e))
	       (e2 (nth 2 e))
	       (e3 (nth 3 e)))
	   (display "(if ")
	   (print-arith e1)
	   (display " then ")
	   (print-arith e2)
	   (display " else ")
	   (print-arith e3)
	   (display ")")))
	((any-of-tagged-list? e 'let 'letrec)
	 (let* ((e1 (nth 1 e))
		(e2 (nth 2 e)))
	   (if (tagged-list? 'let e)
	       (display "(let ")
	       (display "(let rec "))
	   (display ". = ")
	   (print-arith e1)
	   (display " in ")
	   (print-arith e2)
	   (display ")")))
	((tagged-list? 'fun e)
	 (display "fun . -> ")
	 (print-arith (nth 1 e)))
	((tagged-list? 'close e)
	 (display "(")
	 (print-env (reverse (nth 1 e)))
	 (display ")")
	 (display "[")
	 (print-arith (list 'fun (nth 2 e)))
	 (display "]"))
	((tagged-list? 'rec e)
	 (let ((env (rec-env e))
	       (fn (rec-fn e)))
	   (display "(")
	   (print-env (reverse env))
	   (display ")[rec . = ")
	   (print-arith fn)
	   (display "]")))
	((tagged-list? 'sharp e)
	 (display "#")
	 (display (cadr e)))
	((list? e)
	 (display "(")
	 (print-arith (car e))
	 (display " (")
	 (print-arith (cadr e))
	 (display "))"))
	(else
	 (display e))))

(define (print-judge judge)
  (cond ((tagged-list? 'evalto judge)
	 (let ((env (nth 1 judge))
	       (expr (nth 2 judge))
	       (val (nth 3 judge)))
	   (print-env (reverse env))
	   (display " |- ")
	   (print-arith expr)
	   (display " evalto ")
	   (print-arith val)))
	(else (let ((op (car judge))
		    (n1 (cadr judge))
		    (n2 (caddr judge))
		    (n3 (cadddr judge)))
		(display n1)
		(display " ")
		(if (eq? op 'less)
		    (display " less than ")
		    (display op))
		(display " ")
		(display n2)
		(display " is ")
		(display n3)))))

(define (print-derived derived)
  (let ((judge (cadr derived))
	(reason (caddr derived)))
    (print-judge judge)
    (display " by ")
    (let ((rule (car reason))
	  (children-derived (cdr reason)))
      (display rule)
      (display " {")
      (print-children-derived children-derived)
      (display "}"))))

(define (print-children-derived children)
  (if (null? children)
      "{}"
      (let loop ((children children))
	(if (null? (cdr children))
	    (print-derived (car children))
	    (begin (print-derived (car children))
		   (display ";")
		   (newline)
		   (print-children-derived (cdr children)))))))

(define (make-answer judge)
  (println "==================")
  (print-derived (derive judge))
  (newline)
  (println "=================="))

(define (make-input env expr val)
  `(evalto ,(reverse env) ,expr ,val))

(define q55 (make-input '(true 4)
			'(if (sharp 2) (+ (sharp 1) 1) (- (sharp 1) 1))
			5))

(define q57 (make-input '()
			'(let (* 3 3) (let (* 4 (sharp 1)) (+ (sharp 2) (sharp 1))))
			45))

(define q59 (make-input '(3)
			'(let (* (sharp 1) 2)
			   (+ (sharp 1) (sharp 1)))
			12))

(define q61 (make-input '()
			'(let (let (- 3 2)
				(* (sharp 1) (sharp 1)))
			   (let 4
			       (+ (sharp 2)
				  (sharp 1))))
			5))

(define q63 (make-input '()
			'(let 2
			     (fun (+ (sharp 1) (sharp 2))))
			'(close (2) (+ (sharp 1) (sharp 2)))))

(define q65 (make-input '()
			'(let (fun (+ ((sharp 1) 3)
				      ((sharp 1) 4)))
			   ((sharp 1) (fun (* (sharp 1) (sharp 1)))))
			25))

(define q67 (make-input '()
			'(let 3
			     (let (fun (* (sharp 1) (sharp 2)))
			       (let 5
				   ((sharp 2) 4))))
			12))

(define q69 (make-input '()
			'(letrec (fun (if (< (sharp 1) 2)
					  1
					  (* (sharp 1)
					     ((sharp 2) (- (sharp 1) 1)))))
			   ((sharp 1) 3))
			6))


(map make-answer (list q55 q57 q59 q61 q63 q65 q67 q69))
