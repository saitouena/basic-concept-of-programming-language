(load "./utils.scm")

;; peano nat
'(Z)
'(S S Z)

;; judgement

;; Z plus S Z is S Z
'(plus (Z) (S Z) (S Z))

;; S Z times S S Z is S S Z
'(times (S Z) (S S Z) (S S Z))

;; Z times S Z is Z by T-Zero
'(derived
  ;; judge
  (times (Z) (S Z) (Z))
  ;; by
  (T-Zero ;;rule-name
   ;; child judgement
   ))

;; proof of
;; S Z plus Z is S Z
'(derived
  (plus (S Z) (Z) (S Z))
  (P-Succ
   (derived
    (plus (Z) (Z) (Z))
    (P-Zero))))

(define (make-peano-nat n)
  (if (= n 0)
      '(Z)
      (cons 'S (make-peano-nat (- n 1)))))

(define (int-from-peano pn)
  (dec (length pn)))

;; judge is like (plus (S Z) (Z) (S Z))
;; assume that judge is true under Nat system.
;; so no error handling for now
(define (derive judge)
  (cond ((tagged-list? 'plus judge)
	 (let ((n1 (cadr judge))
	       (n2 (caddr judge))
	       (n3 (cadddr judge)))
	   (if (and (tagged-list? 'S n1))
	       `(derived 	       ;; apply P-Succ
		 ,judge
		 (P-Succ
		  ,(derive (list 'plus (cdr n1) n2 (cdr n3)))))
	       `(derived
		 ,judge
		 (P-Zero)))))
	((tagged-list? 'times judge)
	 (let* ((sn1 (nth 1 judge))
		(n2 (nth 2 judge))
		(n4 (nth 3 judge)))
	   (if (and (tagged-list? 'S sn1))
	       (let* ((n1 (cdr sn1))
		      (n3 (make-peano-nat (* (int-from-peano n1) (int-from-peano n2))))) ;; TODO: n3 = n1 * n2
		 `(derived 	       ;; apply T-Succ
		   ,judge
		   (T-Succ
		    ,(derive (list 'times n1 n2 n3))
		    ,(derive (list 'plus n2 n3 n4)))))
	       `(derived
		 ,judge
		 (T-Zero)))))
	(else (error "cannot derive:" judge))))

(define (repeat-s-n s n)
  (if (= n 0)
      #t
      (begin (display s)
	     (repeat-s-n s (- n 1)))))

(define (print-nat n)
  (let ((paren-num (- (length n) 1)))
    (repeat-s-n "S(" paren-num)
    (display "Z")
    (repeat-s-n ")" paren-num)))

(define (print-judge judge)
  (let ((op (car judge))
	(n1 (cadr judge))
	(n2 (caddr judge))
	(n3 (cadddr judge)))
    (print-nat n1)
    (display " ")
    (display op)
    (display " ")
    (print-nat n2)
    (display " is ")
    (print-nat n3)))

(define (print-derived derived)
  (let ((judge (cadr derived))
	(reason (caddr derived)))
    (print-judge judge)
    (display " by ")
    (let ((rule (car reason))
	  (children-derived (cdr reason)))
      (display rule)
      (display " {")
      (print-children-derived children-derived)
      (display "}"))))

(define (print-children-derived children)
  (if (null? children)
      "{}"
      (let loop ((children children))
	(if (null? (cdr children))
	    (print-derived (car children))
	    (begin (print-derived (car children))
		   (display ";")
		   (print-children-derived (cdr children)))))))

;; TODO: add parser that parse 'Z plus Z is Z'
(define (make-answer judge)
  (print-derived (derive judge))
  (newline))

;; q 1.4 from textbook
(define judge-q-1-4-1 '(plus (S S S Z) (S Z) (S S S S Z)))
(define judge-q-1-4-2 '(plus (S Z) (S S S Z) (S S S S Z)))

(define q1 '(plus (Z) (Z) (Z)))
(define q2 '(plus (Z) (S S Z) (S S Z)))
(define q3 '(plus (S S Z) (Z) (S S Z)))
(define q4 '(plus (S Z) (S S S Z) (S S S S Z)))
(define q5 '(times (Z) (S S Z) (Z)))
(define q6 '(times (S S Z) (Z) (Z)))
(define q7 '(times (S S Z) (S Z) (S S Z)))
(define q8 '(times (S S Z) (S S Z) (S S S S Z)))
