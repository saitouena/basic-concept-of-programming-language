(load "./utils.scm")

;; judgement

;; Z plus S Z is S Z
'(plus (Z) (S Z) (S Z))

;; S Z times S S Z is S S Z
'(times (S Z) (S S Z) (S S Z))

;; Z times S Z is Z by T-Zero
'(derived
  ;; judge
  (times (Z) (S Z) (Z))
  ;; by
  (T-Zero ;;rule-name
   ;; child judgement
   ))

;; proof of
;; S Z plus Z is S Z
'(derived
  (plus (S Z) (Z) (S Z))
  (P-Succ
   (derived
    (plus (Z) (Z) (Z))
    (P-Zero))))

(define (make-peano-nat n)
  (if (= n 0)
      '(Z)
      (cons 'S (make-peano-nat (- n 1)))))

(define (int-from-peano pn)
  (dec (length pn)))

;; judge is like (plus (S Z) (Z) (S Z))
;; assume that judge is true under Nat system.
;; so no error handling for now
(define (derive judge)
  (let* ((n1 (nth 1 judge))
	 (n2 (nth 2 judge))
	 (pn1 (int-from-peano n1))
	 (pn2 (int-from-peano n2)))
    (if (= (+ pn1 1) pn2)
	`(derived
	  (less ,n1 ,n2)
	  (L-Succ))
	`(derived
	  (less ,n1 ,n2)
	  (L-SuccR
	   ,(derive (list 'less n1 (make-peano-nat (- pn2 1)))))))))

(define (repeat-s-n s n)
  (if (= n 0)
      #t
      (begin (display s)
	     (repeat-s-n s (- n 1)))))

(define (print-nat n)
  (let ((paren-num (- (length n) 1)))
    (repeat-s-n "S(" paren-num)
    (display "Z")
    (repeat-s-n ")" paren-num)))

(define (print-judge judge)
  (let ((op (car judge))
	(n1 (cadr judge))
	(n2 (caddr judge)))
    (print-nat n1)
    (display " is less than ")
    (print-nat n2)))

(define (print-derived derived)
  (let ((judge (cadr derived))
	(reason (caddr derived)))
    (print-judge judge)
    (display " by ")
    (let ((rule (car reason))
	  (children-derived (cdr reason)))
      (display rule)
      (display " {")
      (print-children-derived children-derived)
      (display "}"))))

(define (print-children-derived children)
  (if (null? children)
      "{}"
      (let loop ((children children))
	(if (null? (cdr children))
	    (print-derived (car children))
	    (begin (print-derived (car children))
		   (display ";")
		   (print-children-derived (cdr children)))))))

;; TODO: add parser that parse 'Z plus Z is Z'
(define (make-answer judge)
  (print-derived (derive judge))
  (newline))

;; q1.5 from textbook
(make-answer '(less (Z) (S S Z)))
(make-answer '(less (S S Z) (S S S S Z)))

(define q9 '(less (S S Z) (S S S Z)))
(make-answer q9)
(define q12 '(less (S S Z) (S S S S S Z)))
(make-answer q12)
