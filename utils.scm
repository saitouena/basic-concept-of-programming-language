(define (tagged-list? tag judge)
  (and (list? judge) (> (length judge) 0) (equal? tag (car judge))))

(define (some pred . xs)
  (if (null? xs)
      #f
      (or (pred (car xs)) (apply some pred (cdr xs)))))

(define (any-of-tagged-list? judge . tags)
  (apply some (lambda (tag) (tagged-list? tag judge)) tags))

;; (any-of? x 'true 'false)
(define (any-of? x . cs)
  (apply some (lambda (c) (equal? x c)) cs))

(define (get-assoc x alst)
  (cdr (assoc x alst)))

(define (symbol->proc sym)
  (eval sym (interaction-environment)))

(define (nth n lst)
  (if (= n 0)
      (car lst)
      (nth (- n 1) (cdr lst))))

(define (dec x) (- x 1))

(define (make-peano-nat n)
  (if (= n 0)
      '(Z)
      (cons 'S (make-peano-nat (- n 1)))))

(define (int-from-peano pn)
  (dec (length pn)))

(define (repeat-s-n s n)
  (if (= n 0)
      #t
      (begin (display s)
	     (repeat-s-n s (- n 1)))))

(define (print-nat n)
  (let ((paren-num (- (length n) 1)))
    (repeat-s-n "S(" paren-num)
    (display "Z")
    (repeat-s-n ")" paren-num)))

(define (println x)
  (display x)
  (newline))

(define (print-derived judge-printer derived)
  (let ((judge (cadr derived))
	(reason (caddr derived)))
    (judge-printer judge)
    (display " by ")
    (let ((rule (car reason))
	  (children-derived (cdr reason)))
      (display rule)
      (display " {")
      (print-children-derived judge-printer children-derived)
      (display "}"))))

(define (print-children-derived judge-printer children)
  (if (null? children)
      #f
      (let loop ((children children))
	(if (null? (cdr children))
	    (print-derived judge-printer (car children))
	    (begin (print-derived judge-printer (car children))
		   (display ";")
		   (newline)
		   (print-children-derived judge-printer (cdr children)))))))

(define-syntax cond-let
  (syntax-rules (else)
    ((cond-let ((var test) expr ...))
     (let ((var test))
       (if var
	   (begin expr ...))))
    ((cond-let ((var1 test1) expr1 ...) ((var2 test2) expr2 ...) ...)
     (let ((var1 test1))
       (if var1
	   (begin expr1 ...)
	   (cond-let ((var2 test2) expr2 ...) ...))))))

(define-syntax if-let
  (syntax-rules ()
    ((if-let (var test) then else)
     (let ((var test))
       (if var
	   then
	   else)))))

(define-syntax when
  (syntax-rules ()
    ((when test stmt ...)
     (if test
	 (begin stmt ...)))))

;; set-minus
(define (set-minus S1 S2)
  (if (null? S1)
      '()
      (let ((x (car S1)))
	(if (memq x S2)
	    (set-minus (cdr S1) S2)
	    (cons x (set-minus (cdr S1) S2))))))

(define (uniq ls)
  (let loop ((acc '()) (ls ls))
    (if (null? ls)
	acc
	(if (memq (car ls) acc)
	    (loop acc (cdr ls))
	    (loop (cons (car ls) acc) (cdr ls))))))

(define (update alst x v)
  (if (null? alst)
      alst
      (let ([head (car alst)])
	(if (eq? (car head) x)
	    (cons (cons x v) (cdr alst))
	    (cons head (update (cdr alst) x v))))))
